# Contents:
- [Docker](#docker)
- [Heroku CLI](#heroku-cli)

## Useful links
- [Deploy docker on heroku](https://devcenter.heroku.com/categories/deploying-with-docker)

# Docker (on local machine)
#### delete old image and rebuild new one
docker-compose up -d --no-deps --build
#### take down container and network of an image
docker-compose down
#### building Dockerfile (also runs image)
docker-compose up
#### see all running containers
docker ps

---
# Heroku CLI
### notes:
 - 3 types of processes: web, worker, clock. Make sure to use the right process type throughout the below commands (depends on the type of app you are building).
#### logging in
heroku login
heroku container:login
### 1) Using Image from Docker Hub Or...
#### choosing docker image (pre-built) to deploy
docker tag ubuntu registry.heroku.com/softbid-runner/worker
#### pushing image to heroku app
docker push registry.heroku.com/softbid-runner/worker
#### Setting up worker
heroku ps:scale worker=1 --app softbid-runner
#### executing runner
heroku ps:exec --app softbid-runner -d worker.1

### 2) ...Using Dockerfile
heroku container:push worker --app softbid-runner
#### Releasing the image (as new dyno version)
heroku container:release worker --app softbid-runner

#### Open heroku app
heroku open --app softbid-runner

### Extras:
#### Checking worker status
heroku ps --app softbid-runner
#### restarting runner (dyno worker.1)
heroku restart worker.1 --app softbid-runner
#### accessing dyno files though bash
heroku run bash --type=worker --app softbid-runner
#### debugging
heroku logs --tail --app softbid-runner
#### delete a dyno
