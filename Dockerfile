# OS chosen
FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

# where our app will live in the container
WORKDIR /Softainer
#Getting all files (including reqs)
COPY ./Softbid ./
#installing reqs
RUN pip install -r requirements.txt
# making migrations for Django
RUN python manage.py makemigrations
RUN python manage.py migrate

#FOR LOCAL MACHINE #doesn't work on Heroku since it assigns a random port
# running at :8000 inside Docker container NOT our localhost. See docker-compose file
# EXPOSE 8000 
# # runs on port 8000 if nothing specified
# CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"] #also useless on Heroku

#FOR HEROKU: (comment above)
CMD gunicorn Softbid.wsgi:application --bind 0.0.0.0:$PORT
