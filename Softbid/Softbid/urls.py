"""Softbid URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.views.static import serve
from django.conf import settings
from django.conf.urls.static import static

# Prevent Django admin page of having a weird look
admin.autodiscover()
admin.site.enable_nav_sidebar = False

# from .settings import MEDIA_ROOT
urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("homepage.urls")),
    path("", include("project_post.urls")),
    path("", include("bid_post.urls")),
    path("", include("registration.urls")),
    # path("captcha", include("captcha.urls")),
    path("", include("account.urls")),
    path("", include("user_dashboard.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# last line added to serve uploaded files
