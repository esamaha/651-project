from django.urls import path
from . import views

urlpatterns = [
    path('account/', views.AccountPage, name="account-page"),
    path('deleteuser/<slug:_username>',views.DeleteUser),
]
