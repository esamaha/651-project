from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from registration.decorators import *
from .forms import *
from .models import *

#region Helper functions
def GetUser(request):
    return  User.objects.get(username= request.user)
#endregion

@login_required(login_url='login')
def AccountPage(request):
    user = GetUser(request)
    # print(user, user.password, user.email)
    # user.email="edy@mail99.com"
    # user.save()
    # print(user, user.password, user.email)
    if request.method == 'POST': #Edit User (editUser)
        # print(request.POST, request.POST['username']) #WORKS
        form = InputForm(request.POST, instance=user)
        # print(form.is_valid())
        if form.is_valid():
            formdata = form.cleaned_data
            # print(formdata)
            newpassword = formdata['newpass'] #form.cleaned_data.get('newpass')
            # print(newpassword=='')
            form.save() #saves everything, NOT right way for password
            if newpassword!='':
                user.set_password(newpassword)
                user.save()
                #After password change, user is not authenticated anymore so reauthenticate
                user = authenticate(request, username=user.username, password=newpassword)
                login(request, user)
            
            messages.success(request, 'Information edited successfully!')


        return render(request, 'account.html', {'form': form})

    else: # See User data. Logically renders first since first accessed thorugh GET
        # print(user)
        form = InputForm(instance=user) #NO CHECKING INFO HERE, JUST RENDERING (info is checked automatically if passing request.POST as arg)
        # print(form.data) #Empty, and no form.cleaned_data, seems these are only for data POSTed
        return render(request, 'account.html', {'form': form})


@login_required(login_url='login')
def DeleteUser(request, _username):
    user = GetUser(request)
    if str(user) == _username: #if user deleting itself NOT other user
        # print('Deleting user')
        user.delete()
    return redirect(DEST_AFTER_AUTH)