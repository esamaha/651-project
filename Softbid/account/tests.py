from django.test import TestCase, SimpleTestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
import random, string
from .views import *
from .forms import *
from .models import *

# region Helper Func
def RandSlug(size=10):
    return "".join(random.choices(string.ascii_letters + string.digits, k=size))


# endregion


class TestUrls(TestCase):
    # Global class vars that don't need to be reinitialized in setUp (aka before each test):
    account_url = reverse("account-page")
    delete_baseurl = "/deleteuser/"  # needs slug after slash

    def setUp(self):  # this function is ran before every test
        # Client needs to be recreated before each test to be fresh
        self.client = Client()
        # Creating DB entry here makes testing file too slow since not all tests need it

    # region Edit
    def test_Edit_View(self):  # Checking that the proper view is called
        self.assertEquals(
            resolve(self.account_url).func,
            AccountPage,
            msg="Incorrect view function ran in Edit",
        )

    def test_Edit_GET_loggedOUT(self):
        # Testing resp code and template
        # client = Client() #Done in setup for all tests
        response = self.client.get(self.account_url)
        self.assertRedirects(
            response,
            Base_Unauth_REDIRECT("account/"),
            msg_prefix="Account page accessed while UNAUTHENTICATED",
        )

    def test_Edit_GET_loggedIN(self):
        # Creating account
        valid_user = User.objects.create(username="Edy")
        # This will hash the password to be compared later on in Login
        valid_user.set_password("edy")
        valid_user.save()
        self.client.login(username="Edy", password="edy")

        # Accessing info
        response = self.client.get(self.account_url)
        self.assertEquals(
            response.status_code,
            200,
            msg="Responde code NOT 200 when visiting account page",
        )
        self.assertTemplateUsed(
            response,
            "account.html",
            msg_prefix="Incorrect template rendered when getting account page",
        )

    def test_Edit_POST_TrivialInfo(self):
        # Creating account
        valid_user = User.objects.create(username="Edy")
        # This will hash the password to be compared later on in Login
        valid_user.set_password("edy")
        valid_user.save()
        self.client.login(username="Edy", password="edy")
        form = InputForm(
            data={
                "first_name": "Micorsoft",
                "email": "edy@mail3.com",
            }
        )
        self.assertTrue(
            form.is_valid(),
            msg="Valid Edit form NOT accepted when changing trivial data",
        )

        # Checking if data changed
        self.client.post(self.account_url, form.data)
        valid_user = User.objects.get(username="Edy")
        self.assertEquals(
            valid_user.first_name,
            "Micorsoft",
            msg="First name field was NOT changed in valid edit",
        )
        self.assertEquals(
            valid_user.email,
            "edy@mail3.com",
            msg="Email field was NOT changed in valid edit",
        )

    def test_Edit_POST_CorrectChangePass(self):
        # Creating account
        valid_user = User.objects.create(username="Edy")
        # This will hash the password to be compared later on in Login
        valid_user.set_password("edy")
        valid_user.save()
        self.client.login(username="Edy", password="edy")
        form = InputForm(
            data={
                "first_name": "Micorsoft",
                "email": "edy@mail3.com",
                "newpass": "Password123",
                "pass_confirmation": "Password123",
            }
        )
        self.assertTrue(
            form.is_valid(), msg="Valid Edit form NOT accepted when changing password"
        )

        # Checking if data changed
        self.client.post(self.account_url, form.data)
        valid_user = User.objects.get(username="Edy")
        self.assertEquals(
            valid_user.first_name,
            "Micorsoft",
            msg="First name field was NOT changed in valid edit",
        )  # making sure other data was also changed as well
        ischanged = valid_user.check_password("Password123")
        self.assertTrue(ischanged, msg="Password field was NOT changed in valid edit")

    def test_Edit_POST_ShortPassword(self):
        # Creating account
        valid_user = User.objects.create(username="Edy")
        # This will hash the password to be compared later on in Login
        valid_user.set_password("edy")
        valid_user.save()
        self.client.login(username="Edy", password="edy")
        form = InputForm(
            data={
                "first_name": "Micorsoft",
                "email": "edy@mail3.com",
                "newpass": "Ab3",
                "pass_confirmation": "Ab3",
            }
        )
        self.assertFalse(form.is_valid(), msg="Form with Short password accepted")

        # Checking if data changed
        self.client.post(self.account_url, form.data)
        valid_user = User.objects.get(username="Edy")
        ischanged = valid_user.check_password("edy")
        self.assertTrue(ischanged, msg="Password field was changed in INvalid edit")

    def test_Edit_POST_BadConfirmationPass(self):
        # Creating account
        valid_user = User.objects.create(username="Edy")
        # This will hash the password to be compared later on in Login
        valid_user.set_password("edy")
        valid_user.save()
        self.client.login(username="Edy", password="edy")
        form = InputForm(
            data={
                "first_name": "Micorsoft",
                "email": "edy@mail3.com",
                "newpass": "Pssword123",
                "pass_confirmation": "Ab3",
            }
        )
        self.assertFalse(
            form.is_valid(), msg="Form with Bad password confirmation accepted"
        )

        # Checking if data changed
        self.client.post(self.account_url, form.data)
        valid_user = User.objects.get(username="Edy")
        ischanged = valid_user.check_password("edy")
        self.assertTrue(ischanged, msg="Password field was changed in INvalid edit")

    # endregion

    # region Delete
    def test_Delete_View(self):  # Checking that the proper view is called
        randslug = RandSlug()
        self.assertEquals(
            resolve(self.delete_baseurl + randslug).func,
            DeleteUser,
            msg="Incorrect view function ran in Delete",
        )

    def test_Delete_loggedOUT(self):
        # Testing resp code and template
        # client = Client() #Done in setup for all tests
        randslug = RandSlug()
        response = self.client.get(self.delete_baseurl + randslug)
        self.assertRedirects(
            response,
            Base_Unauth_REDIRECT(self.delete_baseurl + randslug),
            msg_prefix="Delete page accessed while UNAUTHENTICATED",
        )

    def test_Delete_OwnAccount(self):
        # Creating account
        valid_user = User.objects.create(username="Edy")
        # This will hash the password to be compared later on in Login
        valid_user.set_password("edy")
        valid_user.save()
        self.client.login(username="Edy", password="edy")

        self.client.get(self.delete_baseurl + valid_user.username)
        # Check if authenticated
        logged_in = self.client.login(username="Edy", password="edy")
        self.assertFalse(logged_in, msg="Logged in AFTER deleting account")

    def test_Delete_OtherAccount(self):
        # Creating 2 accounts
        user1 = {"username": "Edy", "password": "edy"}
        user2 = {"username": "Maher", "password": "maher"}

        valid_user = User.objects.create(username=user1["username"])
        # This will hash the password to be compared later on in Login
        valid_user.set_password(user1["password"])
        valid_user.save()
        valid_user = User.objects.create(username=user2["username"])
        # This will hash the password to be compared later on in Login
        valid_user.set_password(user2["password"])
        valid_user.save()

        # Login as user 1
        # NOTE: user1.password is the hashed password NOT plaintext
        self.client.login(username=user1["username"], password=user1["password"])
        # Attempt to delete user 2 as user 1
        self.client.get(self.delete_baseurl + user2["username"])
        self.client.logout()
        # Check if user 2 still exists
        logged_in = self.client.login(
            username=user2["username"], password=user2["password"]
        )
        self.assertTrue(logged_in, msg="User (1) deleted ANOTHER user (2)")
        self.client.logout()
        # Check if user 1 still exists
        logged_in = self.client.login(
            username=user1["username"], password=user1["password"]
        )
        self.assertTrue(
            logged_in,
            msg="User (1) deleted ITSELF while attempting to delete another user (2)",
        )

    def test_Delete_Nonexisting(self):
        valid_user = User.objects.create(username="Edy")
        # This will hash the password to be compared later on in Login
        valid_user.set_password("edy")
        valid_user.save()
        self.client.login(username="Edy", password="edy")

        User_db_count = User.objects.count()
        randslug = RandSlug()
        self.client.get(self.delete_baseurl + randslug)
        self.assertEquals(
            User_db_count,
            User.objects.count(),
            msg="Number of users in DB before and after deleting nonexisting account is Different",
        )


# endregion
