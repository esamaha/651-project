from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm #Need to use UserChangeForm or something
from django.contrib.auth.models import User
from .models import *


# class HiddenForm(UserCreationForm): # HIDDEN form used to check validation of info like username uniqueness and password validation
#     #custom fields
#     profession_choices = [
#         ('client', 'non-technical Business'),
#         ('bidder', 'Software Developper')
#     ]
#     # group = forms.CharField(max_length=30) #no `choices` argument (unlike in model)
#     profession = forms.ChoiceField(choices=profession_choices)

#     class Meta():
#         model = User #Django import NOT custom model
#         # fields = ('group',) + UserCreationForm.Meta.fields #not working
#         fields = ['password1', 'password2'] # password fields appear in form regardless of whether they are in fields list or not
        
class InputForm(forms.ModelForm): #VISIBLE form used to take input from user that does NOT necessarily want to change all fields => some fields may be empty => would trigger invalid error if using User model here
    #NOTE: this form will send important data to the Hidden form above => fields with same name will receive corresponding data
    # username = forms.CharField(label="Username:", max_length=30)
    # email = forms.EmailField(label="Email:", max_length=30, widget=forms.EmailInput()) #Given inside User modle so no need to initialize it
    first_name = forms.CharField(label="Company Name:", max_length=50, required=False) #NOTE: like email it is included in User, BUT want to change label that's why is specified this field here
    newpass = forms.CharField(label="Password:", max_length=257, widget=forms.PasswordInput(), required=False) #In models (NOT forms) blank=True means not required
    pass_confirmation = forms.CharField(label="Confirm Password:", max_length=257, widget=forms.PasswordInput(), required=False)
    
    class Meta():
        model = User #Important to set beginning data using "instance=user" in view
        fields = ['first_name','email', 'newpass','pass_confirmation']

    #Checking Username uniqueness and password reqs is done afterwards in HiddenForm

    #check password and confirmation are the same
    def clean_pass_confirmation(self): #NOTE: since pass_confirmation field is passed after password field: running below in clean_password will result in confirmation=None coz didn't arrive to it yet
        # cleaned_data = super(InputForm, self).clean()
        password1 = self.cleaned_data.get("newpass")
        if password1!='': #left blank => no intent to change
            #Checking pass reqs
            if len(password1)<8:
                raise forms.ValidationError(message="Password must be at least 8 characters long") #stops validating further

        #Checking if pass and validation are the same
        password2 = self.cleaned_data.get("pass_confirmation")
        # print(password1, password2)
        if password1!=password2:
            raise forms.ValidationError(message="Password and Confirmation do NOT match") #stops validating further
        
        return password1
