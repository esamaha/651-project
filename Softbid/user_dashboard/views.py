from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from registration.decorators import allowed_users
from django.contrib.auth.models import User
from registration.models import UserScore
from project_post.models import Project
from bid_post.models import Bid
from datetime import date

# admin considered client user


@login_required(login_url="login")
def dashboard(request):
    user_group = request.user.groups.all()[0].name

    if user_group in ["client", "admin"]:
        return client_dashboard(request)
    elif user_group in ["bidder"]:
        return bidder_dashboard(request)


@login_required(login_url="login")
@allowed_users(["client", "admin"])
def client_dashboard(request):
    client_id = request.user.id
    try:
        projects = Project.objects.filter(Client_id=client_id)
        for project in projects:
            project.bids = Bid.objects.filter(Project_id=project.Project_id)

            # retrieve bidders' usernames (strings)
            project.bidders = []
            for bid in project.bids:
                bidder_username = User.objects.get(pk=bid.Dev_id).username
                project.bidders.append(bidder_username)

            project.num_bids = len(project.bids)
            if project.num_bids > 0:
                project.top_bidder = project.bidders[0]

            time_delta = project.DueDate - date.today()
            days_delta = time_delta.days + 1
            if days_delta > 0:
                project.days_left = days_delta
            elif days_delta < 0:
                project.days_passed = abs(days_delta)
            else:  # is zero
                project.due_today = True

        projects.num_pending = len([p for p in projects if p.num_bids == 0])
        projects.num_active = len([p for p in projects if p.num_bids > 0])
        projects.num_total_bids = sum([p.num_bids for p in projects])

    except Project.DoesNotExist:
        projects = None

    return render(request, "client_dashboard.html", {"projects": projects})


@login_required(login_url="login")
@allowed_users(["bidder"])
def bidder_dashboard(request):
    bidder_id = request.user.id
    try:
        bids = Bid.objects.filter(Dev_id=bidder_id)

        for bid in bids:
            bid.project = Project.objects.get(pk=bid.Project_id)
            bid.client_name = User.objects.get(pk=bid.project.Client_id).username

            time_delta = bid.project.DueDate - date.today()
            days_delta = time_delta.days + 1
            if days_delta > 0:
                bid.project.days_left = days_delta
            elif days_delta < 0:
                bid.project.days_passed = abs(days_delta)
            else:  # is zero
                bid.project.due_today = True

        bids.num_total_projects = len(bids)

        try:
            bids.bidder_score = UserScore.objects.get(user=request.user).score
            # get bidder rank
            bidder_group_id = request.user.groups.all()[0]
            all_bidders = User.objects.filter(groups=bidder_group_id)
            all_bidder_scores = []
            for bidder in all_bidders:
                score = UserScore.objects.get(user=bidder).score
                all_bidder_scores.append(score)
            all_bidder_scores = sorted(all_bidder_scores, reverse=True)
            bids.bidder_rank = all_bidder_scores.index(bids.bidder_score) + 1
        except UserScore.DoesNotExist:
            bids.bidder_score = 0
            bids.bidder_rank = 0

    except Bid.DoesNotExist:
        bids = None

    return render(request, "bidder_dashboard.html", {"bids": bids})
