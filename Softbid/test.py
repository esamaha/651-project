import shutil
import subprocess, signal, sys, os
import random
from time import sleep
import black  # for pipreqs only (maybe temporary)
import coverage  # for pipreqs only (maybe temporary)

INCLUDE_COVERAGE = True
TESTFILES_DIR = "testfiles/"
COVERAGE_FILE = ".coverage"
COVERAGE_HTML_REPORT_DIR = "htmlcov/"
INFO = "[INFO]"
WARNING = "[WARNING]"
SEP_STR = "###############\n"
SEPARATOR = f"\n\n{SEP_STR*3}\n"
PYTHON = sys.executable  # preserve venv in subprocess on windows (unlike "python")


def fprint(*x):
    print(*x, flush=True)  # flush needed for gitlab-AWS to sync output


# delete old coverage data files and reports (COVERAGE_FILE and COVERAGE_HTML_REPORT_DIR)
def delete_old_coverage():
    try:
        fprint(f"\n{INFO} Removing {COVERAGE_FILE}")
        os.remove(COVERAGE_FILE)
    except OSError:
        fprint(
            f"{WARNING} Failed to remove {COVERAGE_FILE}. Not found. (not really a problem)"
        )

    try:
        fprint(f"\n{INFO} Removing {COVERAGE_HTML_REPORT_DIR}")
        shutil.rmtree(COVERAGE_HTML_REPORT_DIR)
    except OSError:
        fprint(
            f"{WARNING} Failed to remove {COVERAGE_HTML_REPORT_DIR}. Not found. (not really a problem)"
        )


def run_tests():
    fprint(f"\n{INFO} Sleeping 3 seconds to wait for server")
    sleep(3)  # give runserver time to run
    fprint(SEPARATOR)
    fprint(f"{INFO} Running tests")
    with open("features.txt", "r") as f:
        features = [line.strip() for line in f]
        random.shuffle(features)  # in-place
        for feature in features:
            if feature.strip():
                fprint(SEPARATOR)
                fprint(f"{INFO} Testing {feature}\n")
                if INCLUDE_COVERAGE:
                    # -a option appends to the coverage report instead of overwriting
                    # this way we can have report that includes all features
                    subprocess.run(
                        ["coverage", "run", "-a", "manage.py", "test", feature.strip()]
                    )
                else:  # run() spawns a subprocess and waits for it terminate
                    subprocess.run([PYTHON, "manage.py", "test", feature.strip()])

    try:  # delete all files in TESTFILES_DIR
        for f in os.listdir(TESTFILES_DIR):
            os.remove(os.path.join(TESTFILES_DIR, f))
    except:
        fprint(f"{WARNING} Could not delete all files in {TESTFILES_DIR}")


def creat_file(n, filename):
    # create N MB size file
    bigFile = open(filename, "w")
    bigFile.seek(1024 * 1024 * n)
    bigFile.write("test")
    bigFile.close()


def create_test_files():
    if not os.path.exists("testfiles/"):
        os.mkdir("testfiles/")
    creat_file(6, "testfiles/6MB.pdf")
    creat_file(1, "testfiles/1MB.pdf")


def delete_test_files():
    if os.path.exists("/testfiles/"):
        shutil.rmtree("/testfiles/")


def pre_test_exam(folderlist):
    for folder in folderlist:
        if not os.path.exists(folder + "/migrations/"):
            os.makedirs(folder + "/migrations/")
        if not os.path.exists(folder + "/migrations/__init__.py"):
            file = open(folder + "/migrations/__init__.py", "w")
            file.close()
    os.system("python manage.py makemigrations")
    os.system("python manage.py migrate")


if __name__ == "__main__":
    folder_list = ["account", "bid_post", "project_post", "registration"]
    pre_test_exam(folder_list)

    fprint(f"\n{INFO} Running server")
    # Popen() spawns a subprocess without waiting for it to terminate
    # redirect stderr to stdout, and suppress their output (throw it into /dev/null)
    server_process = subprocess.Popen(
        [PYTHON, "manage.py", "runserver"],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.STDOUT,
    )

    delete_old_coverage()
    create_test_files()
    run_tests()
    delete_test_files()

    # generate coverage report as html, usually output is in htmlcov/index.html
    fprint(f"\n{INFO} Generating coverage report")
    subprocess.run(["coverage", "report"])
    fprint(f"\n{INFO} Generating HTML coverage report")
    subprocess.run(["coverage", "html"])

    fprint(f"\n{INFO} Killing runserver process with pid {server_process.pid}")
    env = os.environ  # get dict of environment variables
    if "CI_SERVER_NAME" in env:  # if running on gitlab-ci (AWS)
        sys.exit(0)
    elif sys.platform == "win32":  # if running on windows, locally
        os.kill(server_process.pid, signal.CTRL_C_EVENT)
    else:  # if running on linux, locally
        os.killpg(os.getpgid(server_process.pid), signal.SIGTERM)
    # kills process group, i.e. including any spawned child processes
