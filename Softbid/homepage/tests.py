from unittest import TestCase
from django.urls import reverse, resolve
from django.contrib.auth.models import User, Group
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time, random, string
from selenium import webdriver

login_url = reverse("login")

# region Helper Func
def RandString(size=10):
    return "".join(random.choices(string.ascii_letters + string.digits, k=size))


def CreateUser():  # I don't know why in classmethod the database is not flushed and checks if username is unique with other entries => need to create unique test username to avoid error of duplicate user
    try:
        test_username = RandString()
        valid_user = User.objects.create(username=test_username)
        # This will hash the password to be compared later on in Login
        valid_user.set_password("edy")
        valid_user.save()
        return {"username": test_username, "password": "edy"}
    except:
        return CreateUser()


# endregion

# Create your tests here.
class UITest(TestCase):
    @classmethod
    def setUpClass(cls):  # Before ALL tests
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        cls.driver = webdriver.Chrome(chrome_options=options)

    def setUp(self):  # before EACH test
        self.driver = UITest.driver
        self.driver.get("http://127.0.0.1:8000/home/")  # goto account

    # test the dynamic setting of the 'class="curent-page"' attribute to the nav bar elements
    def test_current_page_home(self):
        page_name = "Home"
        self.driver.get(f"http://127.0.0.1:8000/{page_name.lower()}")
        nav_elements = self.driver.find_elements_by_css_selector("nav ul li a")
        for e in nav_elements:
            if e.get_attribute("innerHTML") == page_name:
                assert "current-page" in e.get_attribute(
                    "class"
                ), f"{page_name} page should have class 'current-page'"
            else:
                assert "current-page" not in e.get_attribute(
                    "class"
                ), f"{e.get_attribute('innerHTML')} page should NOT have class 'current-page'. Only the {page_name} page should have it."

    def test_current_page_dashboard(self):
        page_name = "Dashboard"
        self.driver.get(f"http://127.0.0.1:8000/{page_name.lower()}")
        nav_elements = self.driver.find_elements_by_css_selector("nav ul li a")
        for e in nav_elements:
            if e.get_attribute("innerHTML") == page_name:
                assert "current-page" in e.get_attribute(
                    "class"
                ), f"{page_name} page should have class 'current-page'"
            else:
                assert "current-page" not in e.get_attribute(
                    "class"
                ), f"{e.get_attribute('innerHTML')} page should NOT have class 'current-page'. Only the {page_name} page should have it."

    def test_current_page_explore(self):
        page_name = "Explore"
        self.driver.get(f"http://127.0.0.1:8000/project_list")  # changed when linked
        nav_elements = self.driver.find_elements_by_css_selector("nav ul li a")
        for e in nav_elements:
            if e.get_attribute("innerHTML") == page_name:
                assert "current-page" in e.get_attribute(
                    "class"
                ), f"{page_name} page should have class 'current-page'"
            else:
                assert "current-page" not in e.get_attribute(
                    "class"
                ), f"{e.get_attribute('innerHTML')} page should NOT have class 'current-page'. Only the {page_name} page should have it."

    def test_current_page_partners(self):
        page_name = "Partners"
        self.driver.get(f"http://127.0.0.1:8000/{page_name.lower()}")
        nav_elements = self.driver.find_elements_by_css_selector("nav ul li a")
        for e in nav_elements:
            if e.get_attribute("innerHTML") == page_name:
                assert "current-page" in e.get_attribute(
                    "class"
                ), f"{page_name} page should have class 'current-page'"
            else:
                assert "current-page" not in e.get_attribute(
                    "class"
                ), f"{e.get_attribute('innerHTML')} page should NOT have class 'current-page'. Only the {page_name} page should have it."

    def test_current_page_support(self):
        page_name = "Support"
        self.driver.get(f"http://127.0.0.1:8000/{page_name.lower()}")
        nav_elements = self.driver.find_elements_by_css_selector("nav ul li a")
        for e in nav_elements:
            if e.get_attribute("innerHTML") == page_name:
                assert "current-page" in e.get_attribute(
                    "class"
                ), f"{page_name} page should have class 'current-page'"
            else:
                assert "current-page" not in e.get_attribute(
                    "class"
                ), f"{e.get_attribute('innerHTML')} page should NOT have class 'current-page'. Only the {page_name} page should have it."

    # # test navbar buttons behavior
    def test_navbar_button_home(self):
        base_url = "http://127.0.0.1:8000"
        self.driver.get(base_url)
        page_name = "home/"

        nav_elements = self.driver.find_elements_by_css_selector("nav ul li a")
        for e in nav_elements:
            if e.get_attribute("innerHTML") == page_name.title():
                e.click()
                current_url = self.driver.current_url
                assert (
                    current_url == f"{base_url}/{page_name}"
                ), f"After clicking the {page_name} button in the nav bar, we should be on the /{page_name} page, but we incorrectly were on the following page: {current_url}"
                break

    def test_navbar_button_dashboard(self):
        base_url = "http://127.0.0.1:8000"
        self.driver.get(base_url)
        page_name = "dashboard/"

        nav_elements = self.driver.find_elements_by_css_selector("nav ul li a")
        for e in nav_elements:
            if e.get_attribute("innerHTML") == page_name.title():
                e.click()
                current_url = self.driver.current_url
                assert (
                    current_url == f"{base_url}/{page_name}"
                ), f"After clicking the {page_name} button in the nav bar, we should be on the /{page_name} page, but we incorrectly were on the following page: {current_url}"
                break

    def test_navbar_button_explore(self):
        base_url = "http://127.0.0.1:8000"
        self.driver.get(base_url)
        page_name = "explore/"

        nav_elements = self.driver.find_elements_by_css_selector("nav ul li a")
        for e in nav_elements:
            if e.get_attribute("innerHTML") == page_name.title():
                e.click()
                current_url = self.driver.current_url
                assert (
                    current_url == f"{base_url}/{page_name}"
                ), f"After clicking the {page_name} button in the nav bar, we should be on the /{page_name} page, but we incorrectly were on the following page: {current_url}"
                break

    def test_navbar_button_partners(self):
        base_url = "http://127.0.0.1:8000"
        self.driver.get(base_url)
        page_name = "partners/"

        nav_elements = self.driver.find_elements_by_css_selector("nav ul li a")
        for e in nav_elements:
            if e.get_attribute("innerHTML") == page_name.title():
                e.click()
                current_url = self.driver.current_url
                assert (
                    current_url == f"{base_url}/{page_name}"
                ), f"After clicking the {page_name} button in the nav bar, we should be on the /{page_name} page, but we incorrectly were on the following page: {current_url}"
                break

    def test_navbar_button_support(self):
        base_url = "http://127.0.0.1:8000"
        self.driver.get(base_url)
        page_name = "support/"

        nav_elements = self.driver.find_elements_by_css_selector("nav ul li a")
        for e in nav_elements:
            if e.get_attribute("innerHTML") == page_name.title():
                e.click()
                current_url = self.driver.current_url
                assert (
                    current_url == f"{base_url}/{page_name}"
                ), f"After clicking the {page_name} button in the nav bar, we should be on the /{page_name} page, but we incorrectly were on the following page: {current_url}"
                break

    # test card buttons behavior
    def test_card_button_dashboard(self):
        base_url = "http://127.0.0.1:8000"
        self.driver.get(base_url)
        page_name = "dashboard/"

        card_buttons = self.driver.find_elements_by_css_selector(".card .button")
        for b in card_buttons:
            if b.get_attribute("innerHTML") == page_name.title():
                b.click()
                current_url = self.driver.current_url
                assert (
                    current_url == f"{base_url}/{page_name}"
                ), f"After clicking the {page_name} button on its card, we should be on the /{page_name} page, but we incorrectly were on the following page: {current_url}"
                break

    def test_card_button_explore(self):
        base_url = "http://127.0.0.1:8000"
        self.driver.get(base_url)
        page_name = "explore/"

        card_buttons = self.driver.find_elements_by_css_selector(".card .button")
        for b in card_buttons:
            if b.get_attribute("innerHTML") == page_name.title():
                b.click()
                current_url = self.driver.current_url
                assert (
                    current_url == f"{base_url}/{page_name}"
                ), f"After clicking the {page_name} button on its card, we should be on the /{page_name} page, but we incorrectly were on the following page: {current_url}"
                break

    def test_card_button_partners(self):
        base_url = "http://127.0.0.1:8000"
        self.driver.get(base_url)
        page_name = "partners/"

        card_buttons = self.driver.find_elements_by_css_selector(".card .button")
        for b in card_buttons:
            if b.get_attribute("innerHTML") == page_name.title():
                b.click()
                current_url = self.driver.current_url
                assert (
                    current_url == f"{base_url}/{page_name}"
                ), f"After clicking the {page_name} button on its card, we should be on the /{page_name} page, but we incorrectly were on the following page: {current_url}"
                break

    def test_card_button_support(self):
        base_url = "http://127.0.0.1:8000"
        self.driver.get(base_url)
        page_name = "support/"

        card_buttons = self.driver.find_elements_by_css_selector(".card .button")
        for b in card_buttons:
            if b.get_attribute("innerHTML") == page_name.title():
                b.click()
                current_url = self.driver.current_url
                assert (
                    current_url == f"{base_url}/{page_name}"
                ), f"After clicking the {page_name} button on its card, we should be on the /{page_name} page, but we incorrectly were on the following page: {current_url}"
                break

    # Testing Login/logout base buttons
    def test_Unauth_buttons(self):  # starts unauthenticated coz no session
        base_url = "http://127.0.0.1:8000"
        self.driver.get(base_url)
        # should be there
        try:
            login_button = self.driver.find_element_by_name("login-btn")
        except:
            raise AssertionError("Login button not found when Unauthenticated")
        try:
            signup_button = self.driver.find_element_by_name("signup-btn")
        except:
            raise AssertionError("Singup button not found when Unauthenticated")

        # should not be there
        try:
            logout_button = self.driver.find_element_by_name("logout-btn")
            raise AssertionError("Logout button found when Unauthenticated")
        except:
            pass  # correct

    def test_auth_buttons(self):
        base_url = "http://127.0.0.1:8000"

        # Create user
        test_user = CreateUser()

        # Login
        self.driver.get(base_url + login_url)
        time.sleep(0.5)
        elem = self.driver.find_element_by_id("username")
        elem.send_keys(test_user["username"])
        elem = self.driver.find_element_by_id("password")
        elem.send_keys(test_user["password"])
        elem.send_keys(Keys.RETURN)  # press Enter

        time.sleep(2)
        # should not be there
        try:
            login_button = self.driver.find_element_by_name("login-btn")
            raise AssertionError("Login button found when Authenticated")
        except:
            pass  # correct
        try:
            signup_button = self.driver.find_element_by_name("signup-btn")
            raise AssertionError("Signup button found when Authenticated")
        except:
            pass  # correct

        # should be there
        try:
            logout_button = self.driver.find_element_by_name("logout-btn")
        except:
            raise AssertionError("Logout button not found when Authenticated")

        # delete test user from db
        try:  # in case not deleted by test
            userdb = User.objects.get(username=test_user["username"])
            userdb.delete()
        except:
            pass  # already deleted

    @classmethod
    def tearDownClass(cls):  # after ALL test
        cls.driver.quit()
