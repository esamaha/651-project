from django.urls import path
from . import views

urlpatterns = [
    path("", views.index),
    path("home/", views.home, name="home"),
    path("explore/", views.explore, name="explore"),
    path("partners/", views.partners, name="partners"),
    path("support/", views.support, name="support"),
]
