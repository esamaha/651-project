from django.shortcuts import render, redirect
from registration.decorators import *

from django.urls import reverse


def index(request):
    return redirect(reverse("home"))


def home(request):
    return render(request, "home.html")


def explore(request):
    return render(request, "explore.html")


def partners(request):
    return render(request, "partners.html")


def support(request):
    return render(request, "support.html")
