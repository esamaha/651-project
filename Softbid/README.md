# Software Bidding website

Website that enables companies to insert a project they need, then software companies state how much time and money they require to do it as a "bid".

### Installation:
Requires [Python 3](https://www.python.org/downloads/)

Clone directory to your computer and open your terminal in this directory: `cd "directory_name"/addressbook`

Run `pip install -r requirements.txt` to install requirements.
To update requirements.txt file: goto `651-project/Softbid` directory of project and run in cmd `pipreqs . --force`

## Configuration:
- Dev:
In Softbid.settings.py: Comment last line (heroku.settings) + Debug=true (to see errors)

- Deploy (Heroku): 
In Softbid.settings.py: Uncomment last line (heroku.settings) + Debug=false (for security)


## Usage:
In the project direcotry run the following in order:

`python manage.py makemigrations`

`python manage.py migrate`

`python manage.py runserver 8000`

Then open your browser on localhost:8000
