import random
from locust import HttpUser, task, between


class WebsiteUser(HttpUser):
    wait_time = between(1, 5)

    @task
    def index_page(self):
        self.client.get(url="/")

    @task(1)
    def login_page(self):
        response = self.client.get(url="/login")
        csrftoken = response.cookies["csrftoken"]
        first_response = self.client.post(
            "/login/",
            {
                "username": "NewUser" + str(random.randint(0, 10000000)),
                "password": "anil@123",
            },
            headers={"X-CSRFToken": csrftoken},
        )
        self.client.post(
            "/project_post/",
            {
                "Name": "name1",
                "Description": "desc1",
                "DueDate": "10-12-2022",
            },
            headers={"X-CSRFToken": first_response.cookies["csrftoken"]},
        )
        self.client.post(
            "/bid_post/",
            {
                "Cost": "10",
                "Time": "15-12-2022",
                "Description": "bidtest1",
            },
            headers={"X-CSRFToken": first_response.cookies["csrftoken"]},
        )
        self.client.get("/bid_list")

    @task
    def projectlist_page(self):
        self.client.get(url="/project_list")

    @task
    def signup_page(self):
        self.client.get(url="/signup")
