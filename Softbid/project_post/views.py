from .models import Project
from .forms import ProjectPostingForm

# Create your views here.
from django.shortcuts import HttpResponse

# Create your views here.
from django.shortcuts import render
from urllib.parse import urlparse, parse_qs

# Create your views here.
import sys
import os

PROJ_DIR = "Softbid"
sys.path.append(os.path.join(PROJ_DIR, "registration"))
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from project_post.models import Project
from project_post.forms import ProjectPostingForm
from django.contrib import messages
from registration.decorators import allowed_users
from bid_post.models import Bid
from django.shortcuts import render
from django.contrib import messages
from registration.decorators import *
from django.contrib.auth.models import User
from registration.models import UserScore
#region Helper functions

def get_user_score(request):
    try:
        userscore = UserScore.objects.get(user=request.user)
    except:
        UserScore.objects.create(user=request.user, score=20)
        userscore = UserScore.objects.get(user=request.user)
    return userscore

@login_required(login_url='login')
@allowed_users(["client", "admin"])
def project_post(request):
    if request.method == "GET":
        form = ProjectPostingForm()
        return render(request, "Project_posting.html", {"project_form": form})
    else:
        userscore = get_user_score(request)
        #Cannot resolve keyword 'name' into field. Choices are: customuser, date_joined, email, first_name,
        # groups, id, is_active, is_staff, is_superuser, last_login, last_name, logentry, password,
        # user_permissions, username
        # get data from the form
        form = ProjectPostingForm(request.POST)
        file_obj = request.FILES.get("Requirements")
        if form.is_valid():
            if file_obj == None or file_obj.size < 5 * 1024 * 1024:
                client_id = request.user.id
                project_name = request.POST.get("Name")
                project_description = request.POST.get("Description")
                due_date = request.POST.get("Duedate")
                Project.objects.create(
                    Client_id=client_id,
                    Name=project_name,
                    Description=project_description,
                    DueDate=due_date,
                    Attachments=file_obj,
                )
                userscore.score+=5
                userscore.save()
            else:
                messages.info(request, "Attachment exceed 5MB")
                return render(request, "Project_posting.html", {"project_form": form})
        else:
            messages.info(request, "empty input")
            return render(request, "Project_posting.html", {"project_form": form})
    return HttpResponseRedirect("/dashboard/")


def project_list(request):
    # Listing all projects
    Project_list = Project.objects.all()
    return render(request, "Project_list.html", {"project_list": Project_list})


@login_required(login_url="login")
@allowed_users(["client", "admin"])
def project_modify(request):
    """
    Modify use's post
    """
    project_id = int(parse_qs(urlparse(request.get_full_path()).query)["id"][0])
    try:
        project = Project.objects.get(pk=project_id)
    except:
        return HttpResponse("Project does not exist")
    data = {
        "Name": project.Name,
        "Description": project.Description,
        "Duedate": project.DueDate,
    }
    form = ProjectPostingForm(data)
    userscore = get_user_score(request)

    if request.method == "GET":
        # render by the old input and enable the DeleteButton
        return render(
            request,
            "Project_posting.html",
            {"project_form": form, "DeleteButton": True},
        )
    else:
        if "Delete" in request.POST:
            Project.objects.get(pk=project_id).delete()  # Delete this project
            Bid.objects.filter(
                Project_id=project_id
            ).delete()  # Delete project's Bid
            userscore.score -= 5
            userscore.save()
            return HttpResponseRedirect("/dashboard/")  # Return project list
        else:
            form = ProjectPostingForm(request.POST)  # fill the form
            file_obj = request.FILES.get(
                "Requirements"
            )  # get the file if there is no file file_obj will be None
            if (
                form.is_valid()
            ):  # If there is a Captcha in the form, it will judge whether the code is correct
                if (
                    file_obj is None or file_obj.size < 5 * 1024 * 1024
                ):  # Check the file size
                    project.Name = request.POST.get("Name")
                    project.Description = request.POST.get("Description")
                    project.DueDate = request.POST.get("Duedate")
                    project.Attachments = file_obj
                    project.save()  # Save the project
                    userscore.score-=1
                    userscore.save()
                    return HttpResponseRedirect(
                        "/dashboard/"
                    )  # return project list
                else:
                    messages.info(
                        request, "Attachment exceed 5MB"
                    )  # Notify the file size limit
                    return render(
                        request,
                        "Project_posting.html",
                        {"project_form": form, "DeleteButton": True},
                    )
            else:
                messages.info(
                    request, "Verification code error"
                )  # Verification code error
                return render(
                    request,
                    "Project_posting.html",
                    {"project_form": form, "DeleteButton": True},
                )


def user_list(request):
    path = request.get_full_path()
    if path =="/client_rank/":
        UserType='client'
    else:
        UserType='bidder'
    render_list=[]
    user_list = UserScore.objects.all().order_by('score')
    for item in user_list:
        for group in item.user.groups.all():
            if UserType == str(group):
                render_list.append(item)
                break
    return render(request, "user_list.html", {"user_list": render_list})

