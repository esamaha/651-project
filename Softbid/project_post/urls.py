from django.urls import path
from . import views

urlpatterns = [
    path('project_post/', views.project_post,name="project_post"),
    path('project_modify/', views.project_modify,name="project_modify"),
    path('project_list/', views.project_list,name="project_list"),
    path('client_rank/', views.user_list,name="client_rank"),
    path('bidder_rank/', views.user_list,name="bidder_rank"),

]