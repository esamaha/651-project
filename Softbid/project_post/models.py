from django.db import models
from fileinput import filename
from django.contrib.auth.models import User
from django.db import models
from django.core.exceptions import ValidationError


def content_file_name(instance, filename):
    return "/".join(["attachments", str(instance.Client_id), filename])





# Create your models here.
class Project(models.Model):
    Project_id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=100)
    Description = models.CharField(max_length=1000)
    Client_id = models.BigIntegerField()
    DueDate = models.DateField()
    Attachments = models.FileField(
        upload_to=content_file_name, verbose_name="",
    )


