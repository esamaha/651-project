from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, SimpleTestCase, Client
from django.urls import reverse, resolve
import json
import random
import datetime
# Django Auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group

from .views import *
from .models import *
from registration.forms import LoginForm, SignupForm
from .forms import ProjectPostingForm
from django.utils.dateparse import parse_datetime
import string
from registration.models import UserScore
# UNIT TESTS
import os


def newStr(len):
    seed = string.printable
    str1 = []
    for i in range(len):
        str1.append(random.choice(seed))
    StringS = "".join(str1)
    return StringS


class ProjectPostTest(TestCase):
    def setUp(self):  # this function is ran before every test
        self.client = Client()
        self.project_post_url = reverse("project_post")
        self.project_list_url = reverse("project_list")
        self.bid_post_url = reverse("bid_post")
        self.login_url = reverse("login")
        self.signup_url = reverse("signup")
        self.logout_url = reverse("logout")
        self.client_rank_url = reverse("client_rank")
        self.dashboard_url = reverse("dashboard")

    def test_project_post_auth_user(self):
        self.user_login("client")
        response = self.client.get(
            self.project_post_url, follow=True
        )  # Follow=True means if redirected continue until template rendered
        self.assertTemplateUsed(
            response,
            "Project_posting.html",
            msg_prefix="Not able to access project post page",
        )
        response = self.post_valid_project()
        self.assertRedirects(response, self.dashboard_url)

    def test_project_post_unauth_user(self):
        self.user_login("bidder")
        response = self.client.get(
            self.project_post_url, follow=True
        )  # Follow=True means if redirected continue until template rendered
        self.assertContains(response, "You are not authorized to view this page")

    def test_project_post_unauth_user_force(self):
        self.user_login("bidder")
        response = self.post_valid_project()
        self.assertContains(response, "You are not authorized to view this page",
                            msg_prefix="This form should not be accepted")
        # This is a normal test form

    def test_project_post_with_invald_name(self):
        self.user_login("client")
        response = self.post_invalid_project(NameOversize=True)
        self.assertTemplateUsed(response, "Project_posting.html",msg_prefix="This form has invalid name")

    def test_project_post_with_invald_description(self):
        self.user_login("client")
        response = self.post_invalid_project(DescriptionOversize=True)
        self.assertTemplateUsed(response, "Project_posting.html",msg_prefix="This form has invalid description")

    def test_project_post_with_invald_file(self):
        self.user_login("client")
        response = self.post_invalid_project(RequirementsOversize=True)
        #print(response.content)
        self.assertContains(response,"Attachment exceed 5MB")

    def test_project_post_with_invald_deadline(self):
        self.user_login("client")
        response = self.post_invalid_project(DuedateInvalid=True)
        self.assertTemplateUsed(response, "Project_posting.html",msg_prefix="This form has invalid deadlines")

    def test_project_list(self):
        # It should be available to all
        response = self.client.get(self.project_list_url)
        self.assertEquals(
            response.status_code,
            200,
            msg="Responde code NOT 200 when visiting signup page",
        )
        self.assertTemplateUsed(
            response,
            "Project_list.html",
            msg_prefix="Incorrect template rendered when getting signup page",
        )

    def test_project_modify_accessibility(self):
        self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        response = self.client.get("/project_modify/?id=4")
        self.assertEquals(
            response.status_code,
            200,
            msg="Responde code NOT 200 when visiting project page",
        )
        response = self.client.get("/project_modify/?id=12")
        self.assertContains(response,"Project does not exist")

    def test_project_modify_project(self):
        _,uname = self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        response = self.client.get("/project_modify/?id=4")
        self.assertTrue(response.status_code,200)
        response = self.post_valid_project(withfile=True,Link="/project_modify/?id=4")
        self.assertRedirects(response, self.dashboard_url)
        valid_user = User.objects.get(username=uname)
        userscore = UserScore.objects.get(id = valid_user.id)
        #post 10 project earn 50 points plus initial 20 point and minus 1 point for modify project
        self.assertEquals(userscore.score,20+10*5-1)

    def test_project_modify_project_invalid(self):
        _,uname = self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        response = self.client.get("/project_modify/?id=2")
        self.assertTrue(response.status_code,200)
        response = self.post_invalid_project(NameOversize=True,Link="/project_modify/?id=2")
        self.assertTemplateUsed(response, "Project_posting.html", msg_prefix="This form has invalid name")
        valid_user = User.objects.get(username=uname)
        userscore = UserScore.objects.get(id = valid_user.id)
        #post 10 project earn 50 points plus initial 20 point
        self.assertEquals(userscore.score,20+10*5)

    def test_project_modify_unauth_user_force(self):
        self.user_login("bidder")
        response = self.client.get("/project_modify/?id=2")
        self.assertContains(response, "You are not authorized to view this page",
                            msg_prefix="This form should not be accepted")

    def test_user_score(self):
        _,uname = self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        valid_user = User.objects.get(username=uname)
        userscore = UserScore.objects.get(id = valid_user.id)
        #post 10 project earn 50 points plus initial 20 point
        self.assertEquals(userscore.score,20+10*5)

    def test_user_list(self):
        response = self.client.get(self.client_rank_url)
        self.assertEquals(
            response.status_code,
            200,
            msg="Responde code NOT 200 when visiting user list page",
        )
        self.assertTemplateUsed(
            response,
            "user_list.html",
            msg_prefix="Incorrect template rendered when getting user list page",
        )

    def test_user_delete(self):
        _,uname = self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        ProjectForm = ProjectPostingForm(
            data={
                "Name": newStr(15),
                "Description": newStr(200),
                "Duedate": "2022-10-25",
                "Requirements":"",
                "Delete":"Delete"
            }
        )
        self.client.post("/project_modify/?id=2",ProjectForm.data)

    def test_modify_oversize(self):
        self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        response = self.post_invalid_project(RequirementsOversize=True,Link="/project_modify/?id=2")
        #print(response.content)
        self.assertContains(response,"Attachment exceed 5MB")
    def test_trigger_undefined_behavior(self):
        self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        response = self.post_invalid_project(RequirementsOversize=True,Link="/project_modify/?id=2")
        #print(response.content)
        self.assertContains(response,"Attachment exceed 5MB")

    def test_client_rank(self):
        self.user_login("client")
        for i in range(3):
            self.post_valid_project(withfile=False)
        self.client.logout()
        self.user_login("client")
        for i in range(5):
            self.post_valid_project(withfile=False)
        self.client.logout()
        self.user_login("client")
        for i in range(7):
            self.post_valid_project(withfile=False)
        response = self.client.get(self.client_rank_url)
        self.assertEquals(
            response.status_code,
            200,
            msg="Responde code NOT 200 when visiting user list page",
        )
        self.assertTemplateUsed(
            response,
            "user_list.html",
            msg_prefix="Incorrect template rendered when getting user list page",
        )


    def user_login(self,UserType):
        uname = "NewUser" + str(random.randint(0, 10000000))
        form = SignupForm(
            data={
                "username": uname,
                "password1": "ValidPass909",
                "password2": "ValidPass909",
                "email": "new@testuser2.com",
                "profession": UserType,
            }
        )
        self.assertTrue(
            form.is_valid(), msg="Valid User signup NOT accepted"
        )  # Tested previously, but just extra check

        # Check if authenticated
        response = self.client.post(
            self.signup_url, form.data
        )  # PROBLEM: NOT LOGGING IN
        login = self.client.login(username=uname, password="ValidPass909")
        return login,uname


    def post_valid_project(self, withfile=True,Link=None):
        with open('testfiles/1MB.pdf') as fp:
            ProjectForm = ProjectPostingForm(
                data={
                    "Name": newStr(15),
                    "Description": newStr(200),
                    "Duedate": "2022-10-25",
                    "Requirements": fp if withfile else "",
                }
            )
            self.assertTrue(
                ProjectForm.is_valid(), msg="This should be a valid form with a file"
            )
            response = self.client.post(self.project_post_url if Link is None else Link, ProjectForm.data)
        return response

    def post_invalid_project(self, NameOversize=False, DescriptionOversize=False, RequirementsOversize=False,
                             DuedateInvalid=False,Link=None):
        with open('testfiles/1MB.pdf' if not RequirementsOversize else 'testfiles/6MB.pdf') as fp:
            ProjectForm = ProjectPostingForm(
                data={
                    "Name": newStr(random.randint(1, 100) if not NameOversize else random.randint(100, 1000)),
                    "Description": newStr(
                        random.randint(1, 1000) if not DescriptionOversize else random.randint(1000, 10000)),
                    "Duedate": str(
                        datetime.date.today() + datetime.timedelta(days=1)) if not DuedateInvalid else "2022-13-99",
                    "Requirements": fp,
                }
            )
            response = self.client.post(
                self.project_post_url if Link is None else Link, ProjectForm.data
            )
            return response
