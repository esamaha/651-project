from django.apps import AppConfig


class ProjectPostConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'project_post'
