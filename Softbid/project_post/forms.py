from django import forms
#from captcha.fields import CaptchaField
from django_clamd.validators import validate_file_infection
from django.core.exceptions import ValidationError





class ProjectPostingForm(forms.Form):  # Note that it is not inheriting from forms.ModelForm coz no model specified

    Name = forms.CharField(label="Name", max_length=100,
                           widget=forms.TextInput(attrs={"type": "text",
                                                         "name": "Name",
                                                         "placeholder": "Project name"}
                                                  ))
    Description = forms.CharField(label="Description", max_length=1000,
                                  widget=forms.TextInput(attrs={"type": "text",
                                                               "name": "Description",
                                                               "placeholder": "Project description"}
                                                        ))
    Duedate = forms.DateField(label="Duedate",
                              widget=forms.DateInput(attrs={"placeholder": "Project Due Date",
                                                            "class": "textbox-n",
                                                            "type": "text",
                                                            "onfocus": "(this.type='date')",
                                                            "onblur": "(this.type='text')",
                                                            "name": "Duedate"}))
    Requirements = forms.FileField(label="Requirements", required=False,
                                   validators=[validate_file_infection],
                                   allow_empty_file=True,
                                   widget=forms.FileInput(attrs={"type": "file",
                                                                 "name": "Requirements",
                                                                 "placeholder": "Project requirements"}))
    # Captcha = CaptchaField(label='Captcha')
