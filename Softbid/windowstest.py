import sys, os, threading
from subprocess import run, PIPE  # PIPE to prevent script from outputting to shell
from time import sleep
import black  # for pipreqs only (maybe temporary)

# NOTE: if you are on a UNIX system, use python3 instead of python in following commands


# threads=[]
killthreads = False
# Run server to run functional tests
def RunServer():
    print("[*] Running server")
    # First two gonna be run before running the tests on server
    # run("python manage.py makemigrations", shell=False)
    # run("python manage.py migrate", shell=False)
    # NOTE for below: had to do stderr=PIPE because subshell does NOT run in environment (it runs as if new shell) and this generates errors
    # shell=True to be considered as a command on unix
    run("python manage.py runserver", shell=True, stdout=PIPE, stderr=PIPE)
    # runs infinitely so considered a deamon=> cannot be stopped until main script exists


def RunTests():
    sleep(3)  # give runserver time to run
    print("[*] Running tests")
    with open("features.txt", "r") as f:
        for line in f:
            if line.strip():
                print("Testing", line)
                # try:
                os.system(
                    "python manage.py test " + line
                )  # opens a subshell so errors will be outputtted there => cannot envelop subshell in try except
                # except Exception as e:
                #     print('[!] Could NOT run test '+line)
                #     print('Error: '+str(e))
    # Kill threads when this is done
    # KillThreads() #better to do it outside thread
    try:
        os.remove("testfiles/1MB.dat")
        os.remove("testfiles/1GB.dat")
    except:
        print("Problem: couldn't delete the generated data files")
    global killthreads
    killthreads = True
    return  # end thread


# def KillThreads(): #Cannot join a thread if it runs indefinitely (deamon like runserver)
#     print('[*] Joining threads')
#     for t in threads:
#         t.join() #NOTE: join waits for thread to end but does NOT kill it

# Defining threads
t1 = threading.Thread(target=RunServer)
t1.daemon = True  # Classifying as a daemon coz it runs infinitely. Have to specify this to make it exit when system exists
# # threads.append(t1)
t1.start()

t2 = threading.Thread(target=RunTests)
# threads.append(t2)
t2.start()

# for thread in threads: #starting threads
#     thread.start()

while not killthreads:
    pass 
sys.exit(0)  # Exit main script which automatically kills the daemon
