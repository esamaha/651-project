from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import *


class SignupForm(UserCreationForm):
    #custom fields
    profession_choices = [
        ('client', 'non-technical Business'),
        ('bidder', 'Software Developper')
    ]
    # group = forms.CharField(max_length=30) #no `choices` argument (unlike in model)
    profession = forms.ChoiceField(choices=profession_choices)

    class Meta(UserCreationForm.Meta):
        model = User #Django import NOT custom model
        # fields = ('group',) + UserCreationForm.Meta.fields #not working
        fields = ['profession', 'username', 'email', 'password1', 'password2']
        

class LoginForm(forms.Form): #Note that it is not inheriting from forms.ModelForm coz no model specified
    # NOTE: NO class meta NOR model = Users #CAUSING ERROR coz in Users model Email is unique (needed for signup) which sets false the form validation even if not adding to database
    # Email = forms.EmailField(label="Email:", max_length=30, widget=forms.EmailInput())
    username = forms.CharField(label="Username:", max_length=30)
    password = forms.CharField(label="Password:", max_length=257, widget=forms.PasswordInput())

