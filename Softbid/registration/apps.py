from django.apps import AppConfig

DEST_AFTER_AUTH = "/home/"  # used for redirect after authentication in views and decorators (and tests)


def Base_Unauth_REDIRECT(next):
    if next[0]!='/':
        path = '/'+next
    else:
        path = next
    return "/login/?next="+str(path)

class RegistrationConfig(AppConfig):
    name = "registration"
