This application contains functions and decorators to register and authenticate a user.

## Usage:
Insert `from registration.decorators import *` at the top of your script in each application you are developing

### Decorators:
- In your application, to specify that a view function is only accessible by a user that is NOT authenticated, insert `@unauthenticated_user` right above the definition of your function

- In your applications, to check if a user is loggedin to run your function, add`@login_required(login_url='login')` right above the definition of your function.

- In your applications, to specify that your function can only be run by a specific group (ex: admin) to run your function, add `@allowed_users(["admin"])` right above the definition of your function. 

### Groups:
- client (Business seeking Software solutions)
- bidder (Software development team)
- admin (Website admin)

### Accessing data
- To get a specific user data use `request.user.data-you-want`.

- To check which group a user is part of use `request.user.groups.all()[0].name`
