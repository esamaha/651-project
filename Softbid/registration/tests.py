from django.test import TestCase, SimpleTestCase, Client
from django.urls import reverse, resolve
import json
import random
#Django Auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group

from .views import *
from .forms import *
from .models import *
from .apps import DEST_AFTER_AUTH, Base_Unauth_REDIRECT

DEST_UNAUTH = Base_Unauth_REDIRECT('logout/')
# UNIT TESTS
class TestUrls(TestCase):
    #Global class vars that don't need to be reinitialized in setUp (aka before each test):
    
    signup_url = reverse('signup') 
    login_url = reverse('login')
    logout_url = reverse('logout')
    SIGNUP_FORM_FIELDS=4
    LOGIN_FORM_FIELDS=2

    def setUp(self): #this function is ran before every test
        self.client = Client() #Client needs to be recreated before each test to be fresh
        #Creating DB entry here makes testing file too slow since not all tests need it

#region Signup
    def test_Signup_View(self):
        # url = reverse('signup') #Done in setup
        #Checking that the proper view is called
        self.assertEquals(resolve(self.signup_url).func, SignupPage, msg="Incorrect view function ran in signup")

    def test_Signup_GET(self):
        #Testing resp code and template
        # client = Client() #Done in setup for all tests
        response = self.client.get(self.signup_url)
        self.assertEquals(response.status_code, 200, msg="Responde code NOT 200 when visiting signup page")
        self.assertTemplateUsed(response, 'signup.html', msg_prefix="Incorrect template rendered when getting signup page")
        
    def test_Signup_POST_valid(self):
        uname="NewUser"+str(random.randint(0,10000000))
        form = SignupForm(data={
            'username':uname,
            'password1': 'ValidPass909',
            'password2': 'ValidPass909',
            'email': 'new@testuser.com',
            'profession': 'client'
        })
        self.assertTrue(form.is_valid(), msg='Valid User signup NOT accepted')

        # NOTE:Sumbit is tested in authentication test function
        # DOES NOT WORK
        # self.assertEquals(response.user.is_authenticated, True, msg="NOT authenticated") 
        
    def test_Signup_POST_existing_username(self):
        #Create user to test existing user
        valid_user = User.objects.create(username="Edy")
        valid_user.set_password('edy') #This will hash the password to be compared later on in Login
        valid_user.save()

        form = SignupForm(data={
            'username':'Edy',
            'password1': 'ValidPass909',
            'password2': 'ValidPass909',
            'email': 'new@testuser.com',
            'profession': 'client'
        })
        self.assertFalse(form.is_valid(), msg="Accepted existing username in Signup")
        
    
    def test_Signup_POST_invalid_password(self):
        uname="InvalidUser"+str(random.randint(0,10000000))
        # response = self.client.post(self.signup_url, {
        #     'username':uname,
        #     'password1': 'ValidPass909',
        #     'password2': 'INvalidPass808',
        #     'email': 'new@testuser.com',
        # }) #NOTE: post causes CSRF problem, so directly use form
        form = SignupForm(data={
            'username':uname,
            'password1': 'ValidPass909',
            'password2': 'INvalidPass808',
            'email': 'new@testuser.com',
            'profession': 'bidder'
        })
        self.assertFalse(form.is_valid(), msg="Accepted invalid Signup password verification")
        # self.assertTemplateUsed(response, 'signup.html', msg_prefix="Incorrect template rendered for invalid signup in inconsistent password case")
    
    def test_Signup_incomplete_form(self):
        # uname="InvalidUser"+str(random.randint(0,10000000))
        form = SignupForm(data={
        })
        self.assertFalse(form.is_valid(), msg="Accepted empty Signup form")
        self.assertEquals(len(form.errors), self.SIGNUP_FORM_FIELDS, msg="Different incomplete Signup form errors than expected")
        # self.assertTemplateUsed(response, 'signup.html', msg_prefix="Incorrect template rendered for invalid signup in inconsistent password case")

    def test_authenticated_afterSignup(self): #NOTE: NO request.user.is_authenticated here
        # Signup
        uname="NewUser"+str(random.randint(0,10000000))
        form = SignupForm(data={
            'username':uname,
            'password1': 'ValidPass909',
            'password2': 'ValidPass909',
            'email': 'new@testuser2.com',
            'profession': 'client'
        })
        self.assertTrue(form.is_valid(), msg='Valid User signup NOT accepted') #Tested previously, but just extra check
  
        #Check if authenticated
        response = self.client.post(self.signup_url, form.data) #PROBLEM: NOT LOGGING IN
        # self.assertEquals(response.status_code, 302) #302 means redirected
        logged_in = self.client.login(username=uname, password="ValidPass909")  #If it works, then form submitted successfully
        # Check if authenticated
        self.assertTrue(logged_in, msg='Not able to authenticate after valid signup')
        # self.assertRedirects(response, DEST_AFTER_AUTH, status_code=200, msg_prefix="NO redirection to index.html after successful Login (No authentication)") 

        
        # Check if Login page can still be accessed
        response = self.client.get(self.login_url, follow=True) #Follow=True means if redirected continue until template rendered
        self.assertRedirects(response, DEST_AFTER_AUTH, msg_prefix="Login page accessed AFTER Signup authentication")
    
        # Check if Signup page can still be accessed
        response = self.client.get(self.signup_url, follow=True) #Follow=True means if redirected continue until template rendered
        self.assertRedirects(response, DEST_AFTER_AUTH, msg_prefix="Signup page accessed AFTER Signup authentication")
 
        # Check if Logout works
        response = self.client.get(self.logout_url)
        self.assertRedirects(response, DEST_AFTER_AUTH, msg_prefix="NO redirection to index.html after Logout")
    

#endregion

#region Login
    def test_Login_View(self):
        # url = reverse('login') #Done in setup
        #Checking that the proper view is called
        self.assertEquals(resolve(self.login_url).func, LoginPage, msg="Incorrect view function ran in login")

    def test_Login_GET(self):
        #Testing resp code and template
        # client = Client() #Done in setup for all tests
        response = self.client.get(self.login_url)
        self.assertEquals(response.status_code, 200, msg="Responde code NOT 200 when visiting login page")
        self.assertTemplateUsed(response, 'login.html', msg_prefix="Incorrect template rendered when getting login page")
        
    def test_Login_POST_valid(self):
        # Create user
        valid_user = User.objects.create(username="Edy")
        valid_user.set_password('edy') #This will hash the password to be compared later on in Login
        valid_user.save()

        form = LoginForm(data={
            'username':'Edy',
            'password': 'edy',
        })
        self.assertTrue(form.is_valid(), msg="Valid Login NOT accepted")

        logged_in = self.client.login(username='Edy', password="edy") #NOTE: Doesn't test form submit though :/
        self.assertTrue(logged_in, msg="Not able to Login with valid credentials")

    def test_Login_POST_invalid_username(self):
        # Create user
        valid_user = User.objects.create(username="Edy")
        valid_user.set_password('edy') #This will hash the password to be compared later on in Login
        valid_user.save()

        form = LoginForm(data={
            'username':'Wrong',
            'password': 'edy',
        })
        # self.assertFalse(form.is_valid()) #The form is right. In Login we are not using built-in Django form validation, so must check response
        
        #Making sure posting returns to index (authenticates)
        response = self.client.post(self.login_url, form.data)
        self.assertTemplateUsed(response, 'login.html', msg_prefix="Invalid username in Login did NOT return to Login.html page")

    def test_Login_POST_invalid_password(self): #SAME AS ABOVE
        # Create user
        valid_user = User.objects.create(username="Edy")
        valid_user.set_password('edy') #This will hash the password to be compared later on in Login
        valid_user.save()

        #NOTE: client.post causes CSRF problem, so directly use form
        form = LoginForm(data={
            'username':'Edy',
            'password': 'wrong',
        })
        # self.assertFalse(form.is_valid()) #The form is right. In Login we are not using built-in Django form validation, so must check response
       
        #Making sure posting returns to index (authenticates)
        response = self.client.post(self.login_url, form.data)
        # self.client.login()
        self.assertTemplateUsed(response, 'login.html', msg_prefix="Invalid password in Login did NOT return to Login.html page")
    
    def test_Login_incomplete_form(self):
        # uname="InvalidUser"+str(random.randint(0,10000000))
        form = LoginForm(data={
        })
        self.assertFalse(form.is_valid(), msg="Accepted incomplete Login form")
        self.assertEquals(len(form.errors), self.LOGIN_FORM_FIELDS, msg="Different between incomplete Login form errors than expected")
        # self.assertTemplateUsed(response, 'login.html', msg_prefix="Incorrect template rendered for invalid signup in inconsistent password case")

    def test_authenticated_afterLogin(self):
        # Create user
        valid_user = User.objects.create(username="Edy")
        valid_user.set_password('edy') #This will hash the password to be compared later on in Login
        valid_user.save()
        
        #Login
        form = LoginForm(data={
            'username':'Edy',
            'password': 'edy',
        })
        response = self.client.post(self.login_url, form.data, follow=True) #NOT LOGGING IN
        self.assertEquals(response.status_code, 200) #302 means redirected BUT NOT TEMPLATE
        logged_in = self.client.login(username='Edy', password="edy")  #NOTE: Doesn't test form submit though :/
        # Check if authenticated
        self.assertTrue(logged_in, msg="Not able to Login with valid credentials")

        # Check if Login page can still be accessed
        response = self.client.get(self.login_url, follow=True) #Follow=True means if redirected continue until template rendered
        self.assertRedirects(response, DEST_AFTER_AUTH, msg_prefix="Login page accessed AFTER Login authentication")
    
        # Check if Signup page can still be accessed
        response = self.client.get(self.signup_url, follow=True) #Follow=True means if redirected continue until template rendered
        self.assertRedirects(response, DEST_AFTER_AUTH, msg_prefix="Signup page accessed AFTER Login authentication")
 
        # Check if Logout works
        response = self.client.get(self.logout_url)
        self.assertRedirects(response, DEST_AFTER_AUTH, msg_prefix="NO redirection to index.html after Logout") 
    
#endregion

#region Logout
    def test_Logout_View(self):
        # url = reverse('logout') #Done in setup
        #Checking that the proper view is called
        self.assertEquals(resolve(self.logout_url).func, LogoutPage, msg="Incorrect view function ran in logout")

    def test_NOT_authenticated(self):
        self.client.logout() #just in case it starts loggedin by default
        #Check if Logout redirects to Login if Not auth
        response = self.client.get(self.logout_url)
        self.assertRedirects(response, DEST_UNAUTH, msg_prefix="NO redirection to Login when UNauthenticated user goes to Logout url")
    
        # Check if Login page can still be accessed
        response = self.client.get(self.login_url)
        self.assertTemplateUsed(response, 'login.html', msg_prefix="login.html page NOT rendered AFTER Logout")
    
        # Check if Signup page can still be accessed
        response = self.client.get(self.signup_url)
        self.assertTemplateUsed(response, 'signup.html', msg_prefix="signup.html page NOT rendered AFTER Logout")

    def test_Logout_afterLogin(self):
        # Create user
        valid_user = User.objects.create(username="Edy")
        valid_user.set_password('edy') #This will hash the password to be compared later on in Login
        valid_user.save()

        logged_in = self.client.login(username='Edy', password="edy") #NOTE: Doesn't test form submit though :/
        self.assertTrue(logged_in, msg="Not able to Login with valid credentials") #Tested before but just to check
        self.client.logout()
        #Check if Logout redirects to Login if Not auth
        response = self.client.get(self.logout_url)
        self.assertRedirects(response, DEST_UNAUTH, msg_prefix="NO redirection to Login when after user logs out and visits Logout url")

#endregion
