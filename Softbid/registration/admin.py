import pipreqs
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from .models import *


#region adding custom fields to User table
#NOTE: NOT USED YET, currently using the default GROUPS table of Django
class CustomUserAdmin(admin.StackedInline): #stackedInLine to add fields to existing table (User)
    model = CustomUser
    can_delete=False #cannot delete these fields (without deleting the User)
    verbose_name_plural = 'myCustom'
admin.site.register(CustomUser) #to see custom data in seperate table in Django admin

class NewUser(UserAdmin):
    inlines= (CustomUserAdmin,)

admin.site.unregister(User)
admin.site.register(User, NewUser)
#endregion

