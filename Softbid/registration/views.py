from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import inlineformset_factory

# Auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group

from .forms import *
from .models import *
from .decorators import *
from .apps import DEST_AFTER_AUTH

# region Helper Functions
def CheckLogin(request):  # USED DECORATOR INSTEAD
    if request.user.is_authenticated:
        return True
    else:
        return False


# endregion


@unauthenticated_user
def SignupPage(request):
    # if CheckLogin(request): #if already logged in
    # 	messages.info(request, 'You are already logged in')
    # 	return redirect(DEST_AFTER_AUTH) # take user to home page
    CheckGroupsCreated()  # make sure that groups exist in DB
    form = SignupForm()
    try:
        if request.method == "POST":
            form = SignupForm(request.POST)
            if form.is_valid():
                userdata = form.save()
                group = Group.objects.get(name=form.cleaned_data.get("profession"))
                userdata.groups.add(group)
                # Authenticate
                username = form.cleaned_data.get("username")
                password = form.cleaned_data.get("password1")
                user = authenticate(request, username=username, password=password)
                login(request, user)

                messages.success(request, "Account created successfully!")
                return redirect(DEST_AFTER_AUTH)  # take user to home page
            else:  # invalid form
                return render(
                    request, "signup.html", {"form": form, "error": "Invalid inputs"}
                )

        else:  # GET page
            return render(request, "signup.html", {"form": form})
    except Exception as e:  # error
        return render(
            request,
            "signup.html",
            {"form": form, "error": "Problem occured when registering: " + str(e)},
        )


@unauthenticated_user
def LoginPage(request):
    # if CheckLogin(request): #if already logged in
    # 	messages.info(request, 'You are already logged in')
    # 	return redirect(DEST_AFTER_AUTH) # take user to home page
    CheckGroupsCreated()  # make sure that groups exist in DB
    form = LoginForm()
    try:
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                # email = request.POST.get("Email") #Django authenticate uses username
                username = request.POST.get("username")
                password = request.POST.get("password")
                user = authenticate(request, username=username, password=password)

                if user is not None:
                    login(request, user)
                    return redirect(DEST_AFTER_AUTH)  # take user to home page
                else:  # invalid credentials
                    messages.info(request, "Invalid email or password")
                    return render(
                        request,
                        "login.html",
                        {"form": form, "error": "Invalid username or password"},
                    )

            else:  # invalid form
                return render(
                    request, "login.html", {"form": form, "error": "Invalid inputs"}
                )

        else:  # GET page
            return render(request, "login.html", {"form": form})
    except Exception as e:  # error
        return render(
            request,
            "login.html",
            {"form": form, "error": "Problem occured when registering: " + str(e)},
        )


@login_required(login_url="login")  # decorator instead of using "if CheckLogin==False"...
def LogoutPage(request):
    logout(request)
    return redirect(DEST_AFTER_AUTH)


def UserPage(request):
    return render(request, "profile.html", {})

