from django.db import models
from django.contrib.auth.models import User, AbstractUser

# Create your models here.
class CustomUser(
    models.Model
):  # NOT USED YET, currently using the default GROUPS table of Django
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    # uid= models.IntegerField(primary_key=True, auto_created=True, blank=True) #blank=True means field will not be required in form
    # Name = models.CharField(max_length=20, blank=False)
    # Email = models.EmailField(max_length=30, unique=True)
    group_choices = [
        ("client", "Non-Technical Business"),
        ("bidder", "Software Developer"),
    ]
    group = models.CharField(max_length=30, choices=group_choices)


class UserScore(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    score = models.IntegerField()


# class SoftwareDevelopers(models.Model):
#     sd_id= models.IntegerField(primary_key=True, auto_created=True, blank=True) #blank=True means field will not be required in form
#     name = models.CharField(max_length=20, blank=False)
#     email = models.EmailField(max_length=30, unique=True)

#     rating_choices= list(range(1,6)) # 1 to 5
#     rating = models.IntegerField(choices=rating_choices)

# class Clients(models.Model):
#     client_id= models.IntegerField(primary_key=True, auto_created=True, blank=True) #blank=True means field will not be required in form
#     name = models.CharField(max_length=20, blank=False)
#     email = models.EmailField(max_length=30, unique=True)
#     company_type = models.CharField(max_length=20, blank=False)
