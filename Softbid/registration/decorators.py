from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required #not used in this script but it's a decorator used in all apps. Including it here will allow to only import this file in other apps

# from .views import DEST_AFTER_AUTH #CAUSING error in runserver, maybe coz views is also importing decorators
from registration.apps import DEST_AFTER_AUTH, Base_Unauth_REDIRECT

#region Helper Functions
def CheckGroupsCreated():  # Checks if groups were created in "current version of" database (create them if not)
    current_group = Group.objects.all()
    GROUP_NAMES = {'client', 'bidder', 'admin'}
    group=set()
    for object in current_group:
        group.add(object.name)
    for i in GROUP_NAMES-group:
        try:
            group = Group(name=i)
            group.save()
        except:
            raise Exception("Error occur in creating group")
            pass



#endregion

def unauthenticated_user(view_func): #used as @unauthenticated_user in views
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(DEST_AFTER_AUTH)
        else:
            return view_func(request, *args, **kwargs)
    return wrapper_func

# def authenticated_user(view_func): #NO NEED, WE CAN USE BUILT-IN @login_required(login_url='login')
#     def wrapper_func(request, *args, **kwargs):
#         if request.user.is_authenticated:
#             return view_func(request, *args, **kwargs)
#         else:
#             return redirect('/')
#     return wrapper_func

def allowed_users(allowed_roles=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            CheckGroupsCreated() #make sure that groups exist in DB
            group=None
            if request.user.groups.exists():
                group= request.user.groups.all()[0].name

            if group in allowed_roles:    
                return view_func(request, *args, **kwargs)
            else:
                return HttpResponse('You are not authorized to view this page')
        return wrapper_func
    return decorator
