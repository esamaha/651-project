from django_clamd.validators import validate_file_infection
from django import forms
#from captcha.fields import CaptchaField


class BidPostingForm(forms.Form):  # Note that it is not inheriting from forms.ModelForm coz no model specified
    Cost = forms.IntegerField(label="Cost",
                              widget=forms.NumberInput(attrs={'type': "number",
                                                              'name': "Cost",
                                                              'placeholder': "Cost"}
                                                       ))
    Time = forms.DateField(label="Time",
                           widget=forms.DateInput(attrs={'placeholder': "Project Due Date",
                                                         'class': "textbox-n",
                                                         'type': "text",
                                                         'onfocus': "(this.type='date')",
                                                         'onblur': "(this.type='text')",
                                                         'name': "Time"}
                                                  ))
    Description = forms.CharField(label="Description", max_length=1000,
                                  widget=forms.TextInput(attrs={"type": "text",
                                                                "name": "Description",
                                                                "placeholder": "Project description"}
                                                         ))
    Proposal = forms.FileField(label="Proposal", max_length=300, required=False,
                               widget=forms.FileInput(attrs={'type': "file",
                                                             "name": "Proposal",
                                                             'placeholder': "Project requirements"}
                                                      ),
                               validators=[validate_file_infection],
                               allow_empty_file=True)
    #Captcha = CaptchaField(label='Captcha')
