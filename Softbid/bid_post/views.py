import sys
import os

PROJ_DIR = "Softbid"
sys.path.append(os.path.join(PROJ_DIR, "registration"))

from django.shortcuts import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib import messages

# Create your views here.
from django.shortcuts import render
from .models import Bid
from urllib.parse import urlparse, parse_qs

# Create your views here.
from registration.decorators import allowed_users
from django.contrib.auth.decorators import login_required
from project_post.models import Project
from .forms import BidPostingForm
from registration.models import UserScore


def get_user_score(request):
    try:
        userscore = UserScore.objects.get(user=request.user)
    except:
        UserScore.objects.create(user=request.user, score=20)
        userscore = UserScore.objects.get(user=request.user)
    return userscore

@login_required(login_url="login")
@allowed_users(["bidder", "admin"])
def bid_post(request):
    userscore = get_user_score(request)
    project_id = int(parse_qs(urlparse(request.get_full_path()).query)["id"][0])

    # check old bids:
    oldbid = Bid.objects.filter(Dev_id=request.user.id).filter(Project_id=project_id)
    if len(oldbid) != 0:
        return HttpResponse("You have already post a bid")

    if request.method == "GET":
        form = BidPostingForm()
        return render(request, "Bid_posting.html", {"bid_form": form})
    else:
        userscore = UserScore.objects.get(user=request.user)
        # get data from the form
        form = BidPostingForm(request.POST)
        file_obj = request.FILES.get("Proposal")
        if form.is_valid():
            if file_obj == None or file_obj.size < 5 * 1024 * 1024:
                dev_id = request.user.id
                cost = request.POST.get("Cost")
                if int(cost) < 0:
                    return render(request, "Bid_posting.html", {"bid_form": form})
                time = request.POST.get("Time")
                description = request.POST.get("Description")
                file_obj = request.FILES.get("Proposal")
                try:
                    projects = Project.objects.get(pk=project_id)
                    ##If the project exists, bid will be accepted
                    Bid.objects.create(
                        Project_id=project_id,
                        Dev_id=dev_id,
                        Cost=cost,
                        Time=time,
                        Description=description,
                        Proposal=file_obj,
                    )
                    userscore.score+=5
                    userscore.save()
                    return HttpResponseRedirect("/dashboard/")
                except:
                    return HttpResponse("Invalid Bid")
            else:
                messages.info(request, "Attachment exceed 5MB")
                return render(request, "Bid_posting.html", {"bid_form": form})
        else:
            return render(request, "Bid_posting.html", {"bid_form": form})
        # Writing into the database
        # File objects could be empty


@login_required(login_url="login")
@allowed_users(["bidder", "admin"])
def bid_modify(request):
    bid_id = int(parse_qs(urlparse(request.get_full_path()).query)["id"][0])
    try:
        bid = Bid.objects.get(pk=bid_id)
    except:
        return HttpResponse("You haven't post the bid yet")
    userscore = get_user_score(request)
    data = {
        "Cost": bid.Cost,
        "Time": bid.Time,
        "Description": bid.Description,
        "Proposal": bid.Proposal,
    }
    form = BidPostingForm(data)
    if request.method == "GET":
        # render by the old input
        return render(
            request, "Bid_posting.html", {"bid_form": form, "DeleteButton": True}
        )
    else:
        if "Delete" in request.POST:
            Bid.objects.get(pk=bid_id).delete()  # Delete this project
            userscore.score -= 5
            userscore.save()
            return HttpResponseRedirect("/dashboard/")  # Return project list
        # get data from the form
        else:
            form = BidPostingForm(request.POST)  # fill the form
            file_obj = request.FILES.get(
                "Proposal"
            )  # get the file if there is no file file_obj will be None
            if (
                form.is_valid()
            ):  # If there is a Captcha in the form, it will judge whether the code is correct
                if (
                    file_obj is None or file_obj.size < 5 * 1024 * 1024
                ):  # Check the file size
                    bid.Cost = request.POST.get("Cost")
                    bid.Time = request.POST.get("Time")
                    bid.Description = request.POST.get("Description")
                    bid.Proposal = file_obj
                    bid.save()  # Save the Bid
                    userscore.score -= 1
                    userscore.save()
                    return HttpResponseRedirect("/dashboard/")
                else:
                    messages.info(
                        request, "Attachment exceed 5MB"
                    )  # Notify the file size limit
                    return render(
                        request,
                        "Project_posting.html",
                        {"project_form": form, "DeleteButton": True},
                    )
            else:
                messages.info(request, "Verification code error")
                return render(
                    request,
                    "Bid_posting.html",
                    {"bid_form": form, "DeleteButton": True},
                )


@allowed_users(["bidder", "admin"])
def bid_list(request):
    # Listing all projects
    Bid_list = Bid.objects.all()
    return render(request, "Bid_list.html", {"bid_list": Bid_list})
