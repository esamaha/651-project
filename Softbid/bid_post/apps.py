from django.apps import AppConfig


class BidPostConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bid_post'
