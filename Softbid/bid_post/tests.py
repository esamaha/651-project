

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse

import random
import datetime

from django.contrib.auth.models import User

from registration.forms import LoginForm, SignupForm
from project_post.forms import ProjectPostingForm
from .forms import BidPostingForm
from registration.models import UserScore
import string
def newStr(len):
    seed = string.printable
    str1 = []
    for i in range(len):
        str1.append(random.choice(seed))
    StringS = "".join(str1)
    return StringS
class BidPostTest(TestCase):
    def setUp(self):  # this function is ran before every test
        self.project_post_url = reverse("project_post")
        self.project_list_url = reverse("project_list")
        self.bid_post_url = reverse("bid_post")
        self.login_url = reverse("login")
        self.signup_url = reverse("signup")
        self.logout_url = reverse("logout")
        self.bid_list_url = reverse("bid_list")
        self.dashboard_url = reverse("dashboard")
        self.bidder_rank_url = reverse("bidder_rank")

    def test_unauth_bid(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        response = self.client.get("/bid_post/?id=2",follow=True)  # Follow=True means if redirected continue until template rendered
        self.assertTemplateUsed(response,"login.html")

    def test_unauth_bid_force(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        for i in range(5):
            response = self.post_valid_bid(i)
            self.assertContains(response,"You are not authorized to view this page")

    def test_valid_bid_with_file(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        self.user_login(UserType="bidder")
        response = self.post_valid_bid(1)
        self.assertRedirects(response, self.dashboard_url)

    def test_valid_bid_without_file(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        self.user_login(UserType="bidder")
        for i in range(1,5):
            response = self.post_valid_bid(i,withfile=False)
            self.assertRedirects(response, self.dashboard_url)

    def test_repeat_bid(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        self.user_login(UserType="bidder")
        response = self.post_valid_bid(1, withfile=False)
        self.assertRedirects(response, self.dashboard_url)
        response = self.post_valid_bid(1, withfile=False)
        self.assertContains(response,"You have already post a bid")



    def test_invalid_bid_cost(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        self.user_login(UserType="bidder")
        response = self.post_invalid_bid(1,CostlowerThanzero=True)
        self.assertTemplateUsed(response,"Bid_posting.html")

    def test_invalid_bid_description(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        self.user_login(UserType="bidder")
        response = self.post_invalid_bid(5,DescriptionOversize=True)
        self.assertTemplateUsed(response,"Bid_posting.html")

    def test_bid_modify_accessibility(self):
        self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        self.client.logout()
        self.user_login("bidder")
        for i in range(1,11):
            response = self.post_valid_bid(i, withfile=False)
            self.assertRedirects(response, self.dashboard_url)
        response = self.client.get("/bid_modify/?id=4")
        self.assertEquals(
            response.status_code,
            200,
            msg="Responde code NOT 200 when visiting project page",
        )
        response = self.client.get("/bid_modify/?id=12")
        self.assertContains(response,"You haven't post the bid yet")

    def test_bid_modify(self):
        _,uname = self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        self.client.logout()
        _,uname =self.user_login("bidder")
        for i in range(10):
            self.post_valid_bid(project_id=i+1,withfile=False)
        response = self.client.get("/bid_modify/?id=4")
        self.assertTrue(response.status_code,200)
        response = self.post_valid_bid(project_id=4, withfile=True,Link="/bid_modify/?id=4")
        self.assertRedirects(response, self.dashboard_url)
        valid_user = User.objects.get(username=uname)
        userscore = UserScore.objects.get(id = valid_user.id)
        #post 10 project earn 50 points plus initial 20 point and minus 1 point for modify project
        self.assertEquals(userscore.score,20+10*5-1)

    def test_bid_modify_invalid(self):
        _,uname = self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        response = self.client.get("/project_modify/?id=5")
        self.assertTrue(response.status_code,200)
        self.client.logout()
        _,uname =self.user_login("bidder")
        response = self.post_invalid_bid(5,DescriptionOversize=True)
        self.assertTemplateUsed(response,"Bid_posting.html")
        valid_user = User.objects.get(username=uname)
        userscore = UserScore.objects.get(id = valid_user.id)
        #post 10 project earn 50 points plus initial 20 point
        self.assertEquals(userscore.score,20)

    def test_project_modify_unauth_user_force(self):
        self.user_login("bidder")
        response = self.client.get("/project_modify/?id=2")
        self.assertContains(response, "You are not authorized to view this page",
                            msg_prefix="This form should not be accepted")
    def test_modify_oversize(self):
        _, uname = self.user_login("client")
        for i in range(10):
            self.post_valid_project(withfile=False)
        self.client.logout()
        _, uname = self.user_login("bidder")
        response = self.post_valid_bid(1)
        self.assertRedirects(response, self.dashboard_url)
        response = self.post_invalid_bid(5, ProposalOversize=True,Link="/bid_modify/?id=1")
        with open('testfiles/1MB.pdf') as fp:
            BidForm = BidPostingForm(
                data={
                    "Cost": random.randint(100, 1000),
                    "Time": "2022-10-25",
                    "Description": newStr(200),
                    "Proposal": fp,
                    "Delete": "Delete"
                }
            )
            self.assertTrue(BidForm.is_valid(), msg="This should be a valid form with a file")
            response = self.client.post("/bid_modify/?id=1", BidForm.data)
            self.assertRedirects(response,self.dashboard_url)

    def test_bidder_score(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        self.user_login(UserType="bidder")
        response = self.post_valid_bid(4)
        self.assertRedirects(response, self.dashboard_url)
        response = self.post_valid_bid(7)
        self.assertRedirects(response, self.dashboard_url)

    def test_bidder_oversize(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        self.user_login(UserType="bidder")
        response = self.post_invalid_bid(project_id=3,ProposalOversize=True)
        self.assertTemplateUsed(response,"Bid_posting.html")

    def test_invalid_bid_modified(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        self.user_login(UserType="bidder")
        response = self.post_valid_bid(1)
        self.assertRedirects(response, self.dashboard_url)
        response = self.post_invalid_bid(5, DescriptionOversize=True,Link="/bid_modify/?id=1")
        self.assertTemplateUsed(response,"Bid_posting.html")

    def test_no_project_bid(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        self.user_login(UserType="bidder")
        response = self.post_valid_bid(12)
        self.assertContains(response,"Invalid Bid")

    def test_get_bid_form(self):
        self.user_login(UserType="client")
        for _ in range(10):
            self.post_valid_project()
        self.client.logout()
        self.user_login(UserType="bidder")
        self.client.get("/bid_post/?id=3")

    def test_bidder_rank(self):
        self.user_login("client")
        for i in range(3):
            self.post_valid_project(withfile=False)
        self.client.logout()
        self.user_login("client")
        for i in range(5):
            self.post_valid_project(withfile=False)
        self.client.logout()
        self.user_login("client")
        for i in range(7):
            self.post_valid_project(withfile=False)
        self.client.logout()
        self.user_login("bidder")
        for i in range(7):
            response = self.post_valid_bid(i)
        response = self.client.get(self.bidder_rank_url)
        self.assertEquals(
            response.status_code,
            200,
            msg="Responde code NOT 200 when visiting user list page",
        )
        self.assertTemplateUsed(
            response,
            "user_list.html",
            msg_prefix="Incorrect template rendered when getting user list page",
        )
    def test_bid_list(self):
        self.user_login("bidder")
        self.client.get(self.bid_list_url)



    def post_valid_project(self, withfile=True):
        upload_file = open("testfiles/1MB.pdf", "rb")
        file_dict = {"file": SimpleUploadedFile(upload_file.name, upload_file.read())}
        ProjectForm = ProjectPostingForm(
            data={
                "Name": newStr(15),
                "Description": newStr(200),
                "Duedate": "2022-10-25",
                "Requirements": file_dict if withfile else "",
            }
        )
        self.assertTrue(
            ProjectForm.is_valid(), msg="This should be a valid form with a file"
        )
        response = self.client.post(
            self.project_post_url, ProjectForm.data
        )
        return response

    def user_login(self,UserType):
        uname = "NewUser" + str(random.randint(0, 10000000))
        form = SignupForm(
            data={
                "username": uname,
                "password1": "ValidPass909",
                "password2": "ValidPass909",
                "email": "new@testuser2.com",
                "profession": UserType,
            }
        )
        self.assertTrue(
            form.is_valid(), msg="Valid User signup NOT accepted"
        )  # Tested previously, but just extra check

        # Check if authenticated
        response = self.client.post(
            self.signup_url, form.data
        )  # PROBLEM: NOT LOGGING IN
        login = self.client.login(username=uname, password="ValidPass909")
        return login,uname

    def post_invalid_bid(self, project_id, CostlowerThanzero=False, DuedateInvalid=False,
                         DescriptionOversize=False, ProposalOversize=False,Link=None):
        with open('testfiles/1MB.pdf' if not ProposalOversize else 'testfiles/6MB.pdf') as fp:
            BidForm = BidPostingForm(
                data={
                    "Cost": random.randint(100, 1000) if not CostlowerThanzero else random.randint(-100, -10),
                    "Time": str(
                        datetime.date.today() + datetime.timedelta(days=1)) if not DuedateInvalid else "2022-13-99",
                    "Description": newStr(
                        random.randint(1, 1000) if not DescriptionOversize else random.randint(1000, 10000)),
                    "Proposal": fp,
                }
            )
            response = self.client.post("/bid_post/?id=" + str(project_id) if Link is None else Link, BidForm.data
                                        )  # PROBLEM: NOT LOGGING IN
            return response

    def post_valid_bid(self, project_id, withfile=True,Link=None):
        with open('testfiles/1MB.pdf') as fp:
            BidForm = BidPostingForm(
                data={
                    "Cost": random.randint(100, 1000),
                    "Time": "2022-10-25",
                    "Description": newStr(200),
                    "Proposal": fp if withfile else "",
                }
            )
            self.assertTrue(BidForm.is_valid(), msg="This should be a valid form with a file")
            response = self.client.post("/bid_post/?id=" + str(project_id) if Link is None else Link, BidForm.data
                                        )
            return response
    # TODO:Notice there is a leak in security, it will be attack by DDoS
