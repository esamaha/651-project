from django.db import models
from fileinput import filename


def content_file_name(instance, filename):
    return "/".join(["proposals", str(instance.Dev_id), filename])


# Create your models here.
class Bid(models.Model):
    Bid_id = models.AutoField(primary_key=True)
    Project_id = models.IntegerField()
    Dev_id = models.IntegerField()
    Cost = models.IntegerField()
    Time = models.DateField()
    Description = models.CharField(max_length=1000)
    Proposal = models.FileField(upload_to=content_file_name)
