from django.urls import path
from . import views

urlpatterns = [
    path("bid_post/", views.bid_post, name="bid_post"),
    path("bid_modify/", views.bid_modify, name="bid_post"),
    path("bid_list/", views.bid_list, name="bid_list"),
]
