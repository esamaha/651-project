# Functional tests

This folder is used for Front-end behavioral tests, to test for things that cannot be checked on the backend

NOTE: before running the tests, make sure to insert the `chromdriver.exe` your browser is compatible with.
Keep in mind we will replace the chromedriver by a CLI compatible with server CI/CD.

## Usage
To add a test for your feature, create a new file inside this folder called `test_feature-name.py`

To run all tests run in command line `python python manage.py test functional_tests`.
