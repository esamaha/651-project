from selenium.webdriver.chrome.options import Options
import unittest
import random
from project_post import models
from selenium import webdriver
from django.contrib.auth.models import User, Group
from django.urls import reverse, resolve
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class ProjectListUITest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("\nTesting projectlist_ui")

    def setUp(self):
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)

        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()

            datadb = models.Project.objects.get(Name="projectlistuitest")
            datadb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted

        valid_user = User.objects.create(username="navjeet")
        valid_user.set_password("navj@123")
        a = Group.objects.get(name="client")
        a.user_set.add(valid_user)
        valid_user.save()

    def test_missing_projectName(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_post/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "Name"))
        )
        self.driver.find_element_by_name("Name").send_keys("projectlistuitest")
        self.driver.find_element_by_name("Description").send_keys("demo project")
        self.driver.find_element_by_name("Duedate").click()
        self.driver.find_element_by_name("Duedate").send_keys("10-12-2022")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_list/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.ID, "example"))
        )
        num_rows = len(
            self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr')
        )
        num_cols = len(
            self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr[1]/td')
        )
        if num_rows == 0:
            pass
        elif num_rows == 1:
            if (
                self.driver.find_element_by_xpath('//*[@id="example"]/tbody/tr/td').text
                == "No data available in table"
            ):
                self.assertEquals(
                    self.driver.find_element_by_xpath(
                        '//*[@id="example"]/tbody/tr/td'
                    ).text,
                    "No data available in table",
                )
            else:
                # specific_col = random.randint(1, num_cols)
                specific_col = 1
                before_XPath = '//*[@id="example"]/tbody/tr['
                aftertd_XPath = "]/td["
                aftertr_XPath = "]"
                FinalXPath = (
                    before_XPath
                    + str(num_rows)
                    + aftertd_XPath
                    + str(specific_col)
                    + aftertr_XPath
                )
                a = self.driver.find_element_by_xpath(FinalXPath).text
                self.driver.find_element_by_xpath(
                    '//*[@id="example_filter"]/label/input'
                ).send_keys(a)
        else:
            specific_row = random.randint(1, num_rows)
            # specific_col = random.randint(1,num_cols)
            specific_col = 1
            before_XPath = '//*[@id="example"]/tbody/tr['
            aftertd_XPath = "]/td["
            aftertr_XPath = "]"
            FinalXPath = (
                before_XPath
                + str(specific_row)
                + aftertd_XPath
                + str(specific_col)
                + aftertr_XPath
            )
            a = self.driver.find_element_by_xpath(FinalXPath).text
            self.driver.find_element_by_xpath(
                '//*[@id="example_filter"]/label/input'
            ).send_keys(a)
            new_num_row = 1
            new_num_col = 1
            new_before_XPath = '//*[@id="example"]/tbody/tr['
            new_aftertd_XPath = "]/td["
            new_aftertr_XPath = "]"
            new_FinalXPath = (
                new_before_XPath
                + str(new_num_row)
                + new_aftertd_XPath
                + str(new_num_col)
                + new_aftertr_XPath
            )
            b = self.driver.find_element_by_xpath(new_FinalXPath).text
            self.assertEquals(a, b)

    @classmethod
    def tearDownClass(self):
        try:
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
            datadb = models.Project.objects.get(Name="projectlistuitest")
            datadb.delete()
        except:
            pass

    def tearDown(self):
        self.driver.quit()
        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
            datadb = models.Project.objects.get(Name="projectlistuitest")
            datadb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted


if __name__ == "__main__":
    unittest.main()
