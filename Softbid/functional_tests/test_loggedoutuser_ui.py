from selenium.webdriver.chrome.options import Options
import unittest
from selenium import webdriver


class LoggedoutuserUITest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("\nTesting loggedoutuser_ui")

    def setUp(self):
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)

    def test_loggedout_bidList(self):
        self.driver.get("http://127.0.0.1:8000/bid_list/")
        self.assertIn(
            self.driver.find_element_by_xpath("/html/body").text,
            "You are not authorized to view this page",
        )

    @classmethod
    def tearDownClass(self):
        pass

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
