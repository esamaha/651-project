from selenium.webdriver.chrome.options import Options
import unittest
import random
from project_post import models
from selenium import webdriver
from django.contrib.auth.models import User, Group
from django.urls import reverse, resolve
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class ProjectListColCountUITest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("\nTesting projectlist_column_count_ui")

    def setUp(self):
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)

        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()

            datadb = models.Project.objects.get(Name="projectlistcolcountuitest")
            datadb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted

        valid_user = User.objects.create(username="navjeet")
        valid_user.set_password("navj@123")
        a = Group.objects.get(name="client")
        a.user_set.add(valid_user)
        valid_user.save()

    def test_missing_projectName(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_post/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "Name"))
        )
        self.driver.find_element_by_name("Name").send_keys("projectlistcolcountuitest")
        self.driver.find_element_by_name("Description").send_keys("demo project")
        self.driver.find_element_by_name("Duedate").click()
        self.driver.find_element_by_name("Duedate").send_keys("10-12-2022")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_list/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.ID, "example"))
        )
        num_cols = len(
            self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr[1]/td')
        )
        self.assertEquals(num_cols, 6)

    @classmethod
    def tearDownClass(self):
        try:
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
            datadb = models.Project.objects.get(Name="projectlistcolcountuitest")
            datadb.delete()
        except:
            pass

    def tearDown(self):
        self.driver.quit()
        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
            datadb = models.Project.objects.get(Name="projectlistcolcountuitest")
            datadb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted


if __name__ == "__main__":
    unittest.main()
