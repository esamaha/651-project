from ast import Assert
import random
from unittest import TestCase
from selenium.webdriver.chrome.options import Options
import unittest
from selenium import webdriver
import time
from django.contrib.auth.models import User, Group
from django.urls import reverse, resolve
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BidPostUITest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("\nTesting bidpost_ui")

    def setUp(self):
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)

        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted

        valid_user = User.objects.create(username="navjeet")
        valid_user.set_password("navj@123")
        a = Group.objects.get(name="bidder")
        a.user_set.add(valid_user)
        valid_user.save()

    def test_open_bidpost(self):

        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_list/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.ID, "example"))
        )
        # self.assertEquals(self.driver.title, "Project_listing")
        num_rows = len(
            self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr')
        )
        if num_rows == 0:
            pass
        elif num_rows == 1:
            if (
                self.driver.find_element_by_xpath('//*[@id="example"]/tbody/tr/td').text
                == "No data available in table"
            ):
                self.assertEquals(
                    self.driver.find_element_by_xpath(
                        '//*[@id="example"]/tbody/tr/td'
                    ).text,
                    "No data available in table",
                )
            else:
                num_cols = len(
                    self.driver.find_elements_by_xpath(
                        '//*[@id="example"]/tbody/tr[1]/td'
                    )
                )
                before_XPath = '//*[@id="example"]/tbody/tr['
                aftertd_XPath = "]/td["
                aftertr_XPath = "]/a"
                FinalXPath = (
                    before_XPath
                    + str(num_rows)
                    + aftertd_XPath
                    + str(num_cols)
                    + aftertr_XPath
                )
                a = self.driver.find_element_by_xpath(FinalXPath).click()
                WebDriverWait(self.driver, 5).until(
                    EC.element_to_be_clickable((By.NAME, "Cost"))
                )
                self.driver.find_element_by_name("Cost").send_keys("")
                self.driver.find_element_by_name("Time").click()
                self.driver.find_element_by_name("Time").send_keys("2022-12-10")
                self.driver.find_element_by_name("Description").send_keys("desc1")
                self.driver.find_element_by_id("submit").click()
                self.assertIn(
                    self.driver.title,
                    "Bid Post",
                )
        else:
            specific_row = random.randint(1, num_rows)
            num_cols = len(
                self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr[1]/td')
            )
            before_XPath = '//*[@id="example"]/tbody/tr['
            aftertd_XPath = "]/td["
            aftertr_XPath = "]/a"
            FinalXPath = (
                before_XPath
                + str(specific_row)
                + aftertd_XPath
                + str(num_cols)
                + aftertr_XPath
            )
            a = self.driver.find_element_by_xpath(FinalXPath).click()
            WebDriverWait(self.driver, 5).until(
                EC.element_to_be_clickable((By.NAME, "Cost"))
            )
            self.driver.find_element_by_name("Cost").send_keys("")
            self.driver.find_element_by_name("Time").click()
            self.driver.find_element_by_name("Time").send_keys("2022-12-10")
            self.driver.find_element_by_name("Description").send_keys("desc1")
            self.driver.find_element_by_id("submit").click()
            self.assertIn(
                self.driver.title,
                "Bid Post",
            )

    # def test_missingcost(self):
    #     self.driver.get("http://127.0.0.1:8000/login/")
    #     # if self.assertEquals(self.driver.title, "Login"):
    #     self.driver.find_element_by_name("username").send_keys("john")
    #     self.driver.find_element_by_name("password").send_keys("john@123")
    #     self.driver.find_element_by_id("submit").click()
    #     self.driver.get("http://127.0.0.1:8000/project_list/")
    #     num_rows = len(self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr'))
    #     num_cols = len(self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr[2]/td'))
    #     if num_rows > 0:
    #         specific_row = random.randint(1, num_rows)
    #         specific_col = random.randint(1,num_cols)
    #     before_XPath = '//*[@id="example"]/tbody/tr['
    #     aftertd_XPath = ']/td['
    #     aftertr_XPath = ']/a'
    #     FinalXPath = before_XPath + str(specific_row) + aftertd_XPath + str(num_cols) + aftertr_XPath
    #     a = self.driver.find_element_by_xpath(FinalXPath).click()
    #     self.driver.find_element_by_name("Cost").send_keys("")
    #     self.driver.find_element_by_name("Time").click()
    #     self.driver.find_element_by_name("Time").send_keys("2022-12-10")
    #     self.driver.find_element_by_name("Description").send_keys("john")
    #     self.driver.find_element_by_id("submit").click()

    @classmethod
    def tearDownClass(self):
        try:
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
        except:
            pass

    def tearDown(self):
        self.driver.quit()
        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted


if __name__ == "__main__":
    unittest.main()
