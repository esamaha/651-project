from unicodedata import name
from project_post import models
from selenium.webdriver.chrome.options import Options
import unittest
from selenium import webdriver
from django.contrib.auth.models import User, Group
from django.urls import reverse, resolve
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class ProjectPostUITest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("\nTesting projectpost_ui")

    def setUp(self):
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)
        # self.test_user = CreateUser()

        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()

            datadb = models.Project.objects.get(Name="projectpostuitest")
            datadb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted

        valid_user = User.objects.create(username="navjeet")
        valid_user.set_password("navj@123")
        a = Group.objects.get(name="client")
        a.user_set.add(valid_user)
        valid_user.save()

    def test_open_projectpost(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_post/")
        self.assertEquals(self.driver.title, "Project Post")

    def test_missing_projectName(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_post/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "Name"))
        )
        self.driver.find_element_by_name("Name").send_keys("")
        self.driver.find_element_by_name("Description").send_keys("demo project")
        self.driver.find_element_by_name("Duedate").click()
        self.driver.find_element_by_name("Duedate").send_keys("2022-12-10")
        self.driver.find_element_by_id("submit").click()
        self.assertEquals(self.driver.find_element_by_id("error").text, "empty input")

    def test_missing_projectDescription(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_post/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "Name"))
        )
        self.driver.find_element_by_name("Name").send_keys("Project1")
        self.driver.find_element_by_name("Description").send_keys("")
        self.driver.find_element_by_name("Duedate").click()
        self.driver.find_element_by_name("Duedate").send_keys("2022-12-10")
        self.driver.find_element_by_id("submit").click()
        self.assertEquals(self.driver.find_element_by_id("error").text, "empty input")

    def test_missing_projectDueDate(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_post/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "Name"))
        )
        self.driver.find_element_by_name("Name").send_keys("Project1")
        self.driver.find_element_by_name("Description").send_keys("demo project")
        self.driver.find_element_by_name("Duedate").click()
        self.driver.find_element_by_name("Duedate").send_keys("")
        self.driver.find_element_by_id("submit").click()
        self.assertEquals(self.driver.find_element_by_id("error").text, "empty input")

    def test_projectPosting(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_post/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "Name"))
        )
        self.driver.find_element_by_name("Name").send_keys("projectpostuitest")
        self.driver.find_element_by_name("Description").send_keys("demo project")
        self.driver.find_element_by_name("Duedate").click()
        self.driver.find_element_by_name("Duedate").send_keys("10-12-2022")
        self.driver.find_element_by_id("submit").click()
        self.assertIn(
            self.driver.title,
            "Project_listing",
        )

    @classmethod
    def tearDownClass(self):
        try:
            userdb = User.objects.get(username="navjeet")
            userdb.delete()

            datadb = models.Project.objects.get(Name="projectpostuitest")
            datadb.delete()
        except:
            pass

    def tearDown(self):
        self.driver.quit()
        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()

            datadb = models.Project.objects.get(Name="projectpostuitest")
            datadb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted


if __name__ == "__main__":
    unittest.main()
