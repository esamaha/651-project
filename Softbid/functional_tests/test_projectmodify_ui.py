from selenium.webdriver.chrome.options import Options
import unittest
import random
from project_post import models
from selenium import webdriver
from django.contrib.auth.models import User, Group
from django.urls import reverse, resolve
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class ProjectModifyUITest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("\nTesting projectmodify_ui")

    def setUp(self):
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)

        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()

            datadb = models.Project.objects.get(Name="projectmodifyuitest0")
            datadb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted

        valid_user = User.objects.create(username="navjeet")
        valid_user.set_password("navj@123")
        a = Group.objects.get(name="client")
        a.user_set.add(valid_user)
        valid_user.save()

    def test_modify_projectName(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_post/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "Name"))
        )
        self.driver.find_element_by_name("Name").send_keys("projectmodifyuitest")
        self.driver.find_element_by_name("Description").send_keys("demo project")
        self.driver.find_element_by_name("Duedate").click()
        self.driver.find_element_by_name("Duedate").send_keys("10-12-2022")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_list/")
        # WebDriverWait(self.driver, 5).until(
        #     EC.element_to_be_clickable((By.ID, "example"))
        # )
        num_rows = len(
            self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr')
        )
        num_cols = len(
            self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr[1]/td')
        )
        if num_rows == 0:
            pass
        elif num_rows == 1:
            if (
                self.driver.find_element_by_xpath('//*[@id="example"]/tbody/tr/td').text
                == "No data available in table"
            ):
                self.assertEquals(
                    self.driver.find_element_by_xpath(
                        '//*[@id="example"]/tbody/tr/td'
                    ).text,
                    "No data available in table",
                )
            else:
                # specific_col = random.randint(1, num_cols)
                specific_col = 6
                before_XPath = '//*[@id="example"]/tbody/tr['
                aftertd_XPath = "]/td["
                aftertr_XPath = "]/a"
                aftertr_XPath1 = "]"
                FinalXPath = (
                    before_XPath
                    + str(num_rows)
                    + aftertd_XPath
                    + str(specific_col)
                    + aftertr_XPath
                )
                specific_col2 = 1
                FinalXPath2 = (
                    before_XPath
                    + str(num_rows)
                    + aftertd_XPath
                    + str(specific_col2)
                    + aftertr_XPath1
                )
                datadb = models.Project.objects.get(Name="projectmodifyuitest")
                projid = datadb.Project_id
                a_before = self.driver.find_element_by_xpath(FinalXPath2).text
                # self.driver.find_element_by_xpath(FinalXPath).click()
                self.driver.get(
                    "http://127.0.0.1:8000/project_modify/?id=" + str(projid)
                )
                WebDriverWait(self.driver, 5).until(
                    EC.element_to_be_clickable((By.NAME, "Name"))
                )
                self.driver.find_element_by_name("Name").send_keys("0")
                self.driver.find_element_by_id("submit").click()
                self.driver.get("http://127.0.0.1:8000/project_list/")
                # WebDriverWait(self.driver, 5).until(
                #     EC.element_to_be_clickable((By.ID, "example"))
                # )
                specific_col1 = 1
                FinalXPath1 = (
                    before_XPath
                    + str(num_rows)
                    + aftertd_XPath
                    + str(specific_col1)
                    + aftertr_XPath1
                )
                a_after = self.driver.find_element_by_xpath(FinalXPath1).text
                a = a_before + "0"
                self.assertEquals(a_after, a)

        else:
            specific_row1 = random.randint(1, num_rows)
            specific_col = 6
            before_XPath = '//*[@id="example"]/tbody/tr['
            aftertd_XPath = "]/td["
            aftertr_XPath = "]/a"
            aftertr_XPath2 = "]"
            temp_col = 1
            projid = 1
            for i in range(1, num_rows + 1):
                TempPath = (
                    before_XPath
                    + str(i)
                    + aftertd_XPath
                    + str(temp_col)
                    + aftertr_XPath2
                )
                if (
                    self.driver.find_element_by_xpath(TempPath).text
                    == "projectmodifyuitest"
                ):
                    specific_row1 = i
                    datadb = models.Project.objects.get(Name="projectmodifyuitest")
                    projid = datadb.Project_id
                    break
            FinalXPath = (
                before_XPath
                + str(specific_row1)
                + aftertd_XPath
                + str(specific_col)
                + aftertr_XPath
            )
            # self.driver.find_element_by_xpath(FinalXPath).click()
            self.driver.get("http://127.0.0.1:8000/project_modify/?id=" + str(projid))
            WebDriverWait(self.driver, 5).until(
                EC.element_to_be_clickable((By.NAME, "Name"))
            )
            self.driver.find_element_by_name("Name").send_keys("0")
            self.driver.find_element_by_id("submit").click()
            self.driver.get("http://127.0.0.1:8000/project_list/")
            # WebDriverWait(self.driver, 5).until(
            #     EC.element_to_be_clickable((By.ID, "example"))
            # )
            specific_col1 = 1
            FinalXPath1 = (
                before_XPath
                + str(specific_row1)
                + aftertd_XPath
                + str(specific_col1)
                + aftertr_XPath2
            )
            a = self.driver.find_element_by_xpath(FinalXPath1).text
            self.assertEquals(a, "projectmodifyuitest0")

    def test_delete_project(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_post/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "Name"))
        )
        self.driver.find_element_by_name("Name").send_keys("projectmodifyuitest")
        self.driver.find_element_by_name("Description").send_keys("demo project")
        self.driver.find_element_by_name("Duedate").click()
        self.driver.find_element_by_name("Duedate").send_keys("10-12-2022")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_list/")
        # WebDriverWait(self.driver, 5).until(
        #     EC.element_to_be_clickable((By.ID, "example"))
        # )
        num_rows = len(
            self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr')
        )
        num_cols = len(
            self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr[1]/td')
        )
        if num_rows == 0:
            pass
        elif num_rows == 1:
            if (
                self.driver.find_element_by_xpath('//*[@id="example"]/tbody/tr/td').text
                == "No data available in table"
            ):
                self.assertEquals(
                    self.driver.find_element_by_xpath(
                        '//*[@id="example"]/tbody/tr/td'
                    ).text,
                    "No data available in table",
                )
            else:
                # specific_col = random.randint(1, num_cols)
                specific_col = 6
                before_XPath = '//*[@id="example"]/tbody/tr['
                aftertd_XPath = "]/td["
                aftertr_XPath = "]/a"
                aftertr_XPath3 = "]"
                FinalXPath = (
                    before_XPath
                    + str(num_rows)
                    + aftertd_XPath
                    + str(specific_col)
                    + aftertr_XPath
                )
                datadb = models.Project.objects.get(Name="projectmodifyuitest")
                projid = datadb.Project_id
                # self.driver.find_element_by_xpath(FinalXPath).click()
                self.driver.get(
                    "http://127.0.0.1:8000/project_modify/?id=" + str(projid)
                )
                WebDriverWait(self.driver, 5).until(
                    EC.element_to_be_clickable((By.NAME, "Name"))
                )
                self.driver.find_element_by_id("Delete").click()
                self.driver.get("http://127.0.0.1:8000/project_list/")
                # WebDriverWait(self.driver, 5).until(
                #     EC.element_to_be_clickable((By.ID, "example"))
                # )
                specific_col1 = 1
                FinalXPath1 = (
                    before_XPath
                    + str(num_rows)
                    + aftertd_XPath
                    + str(specific_col1)
                    + aftertr_XPath3
                )
                a = self.driver.find_element_by_xpath(FinalXPath1).text
                self.assertNotEquals(a, "projectmodifyuitest")

        else:
            specific_row1 = random.randint(1, num_rows)
            specific_col = 6
            before_XPath = '//*[@id="example"]/tbody/tr['
            aftertd_XPath = "]/td["
            aftertr_XPath = "]/a"
            aftertr_XPath2 = "]"
            temp_col = 1
            for i in range(1, num_rows + 1):
                TempPath = (
                    before_XPath
                    + str(i)
                    + aftertd_XPath
                    + str(temp_col)
                    + aftertr_XPath2
                )
                if (
                    self.driver.find_element_by_xpath(TempPath).text
                    == "projectmodifyuitest"
                ):
                    specific_row1 = i
                    datadb = models.Project.objects.get(Name="projectmodifyuitest")
                    projid = datadb.Project_id
                    break
            FinalXPath = (
                before_XPath
                + str(specific_row1)
                + aftertd_XPath
                + str(specific_col)
                + aftertr_XPath
            )
            # self.driver.find_element_by_xpath(FinalXPath).click()
            self.driver.get("http://127.0.0.1:8000/project_modify/?id=" + str(projid))
            WebDriverWait(self.driver, 5).until(
                EC.element_to_be_clickable((By.NAME, "Name"))
            )
            self.driver.find_element_by_id("Delete").click()
            self.driver.get("http://127.0.0.1:8000/project_list/")
            # WebDriverWait(self.driver, 5).until(
            #     EC.element_to_be_clickable((By.ID, "example"))
            # )
            specific_col1 = 1
            FinalXPath1 = (
                before_XPath
                + str(specific_row1)
                + aftertd_XPath
                + str(specific_col1)
                + aftertr_XPath2
            )
            a = self.driver.find_element_by_xpath(FinalXPath1).text
            self.assertNotEquals(a, "projectmodifyuitest")

    @classmethod
    def tearDownClass(self):
        try:
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
            datadb = models.Project.objects.get(Name="projectmodifyuitest0")
            datadb.delete()
        except:
            pass

    def tearDown(self):
        self.driver.quit()
        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
            datadb = models.Project.objects.get(Name="projectmodifyuitest0")
            datadb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted


if __name__ == "__main__":
    unittest.main()
