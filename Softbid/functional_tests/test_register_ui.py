from selenium.webdriver.chrome.options import Options
import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import Select


class Test_Registerui(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("\nTesting register_ui")

    def setUp(self):
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)
        # self.driver = webdriver.Chrome("Softbid\chromedriver.exe")
        # self.driver.implicitly_wait(5)

    def test_register_nopassword(self):
        self.driver.get("http://127.0.0.1:8000/signup/")
        self.driver.find_element_by_name("username").send_keys("mathew")
        self.driver.find_element_by_name("email").send_keys("mathew@example.com")
        self.driver.find_element_by_name("org_name").send_keys("TCS")
        self.driver.find_element_by_name("password1").send_keys("")
        self.driver.find_element_by_name("password2").send_keys("")
        se = Select(self.driver.find_element_by_name("profession"))
        se.select_by_index(1)
        self.driver.find_element_by_id("submit").click()
        password1_message_actual = self.driver.find_element_by_id(
            "password1-error"
        ).text
        password2_message_actual = self.driver.find_element_by_id(
            "password2-error"
        ).text
        self.assertEquals("This field is required.", password1_message_actual)
        self.assertEquals("This field is required.", password2_message_actual)

    def test_register_nousernamepassword(self):
        self.driver.get("http://127.0.0.1:8000/signup/")
        self.driver.find_element_by_name("username").send_keys("")
        self.driver.find_element_by_name("email").send_keys("mathew@example.com")
        self.driver.find_element_by_name("org_name").send_keys("Google")
        self.driver.find_element_by_name("password1").send_keys("")
        self.driver.find_element_by_name("password2").send_keys("")
        se = Select(self.driver.find_element_by_name("profession"))
        se.select_by_index(1)
        self.driver.find_element_by_id("submit").click()
        username_message_actual = self.driver.find_element_by_id("username-error").text
        password1_message_actual = self.driver.find_element_by_id(
            "password1-error"
        ).text
        password2_message_actual = self.driver.find_element_by_id(
            "password2-error"
        ).text
        self.assertEquals("This field is required.", username_message_actual)
        self.assertEquals("This field is required.", password1_message_actual)
        self.assertEquals("This field is required.", password2_message_actual)

    def test_register_commonpassword(self):
        self.driver.get("http://127.0.0.1:8000/signup/")
        self.driver.find_element_by_name("username").send_keys("mathew")
        self.driver.find_element_by_name("email").send_keys("mathew@example.com")
        self.driver.find_element_by_name("org_name").send_keys("Google")
        self.driver.find_element_by_name("password1").send_keys("12345678")
        self.driver.find_element_by_name("password2").send_keys("12345678")
        se = Select(self.driver.find_element_by_name("profession"))
        se.select_by_index(1)
        self.driver.find_element_by_id("submit").click()
        password2_message_actual = self.driver.find_element_by_id(
            "password2-error"
        ).text
        self.assertEquals(
            "This password is too common.\nThis password is entirely numeric.",
            password2_message_actual,
        )

    def test_register_passwordnomatch(self):
        self.driver.get("http://127.0.0.1:8000/signup/")
        self.driver.find_element_by_name("username").send_keys("mathew")
        self.driver.find_element_by_name("email").send_keys("mathew@example.com")
        self.driver.find_element_by_name("org_name").send_keys("Google")
        self.driver.find_element_by_name("password1").send_keys("12345678")
        self.driver.find_element_by_name("password2").send_keys("12345670")
        se = Select(self.driver.find_element_by_name("profession"))
        se.select_by_index(1)
        self.driver.find_element_by_id("submit").click()
        password2_message_actual = self.driver.find_element_by_id(
            "password2-error"
        ).text
        self.assertEquals(
            "The two password fields didn’t match.", password2_message_actual
        )

    def test_register_shortnumericpassword(self):
        self.driver.get("http://127.0.0.1:8000/signup/")
        self.driver.find_element_by_name("username").send_keys("hello")
        self.driver.find_element_by_name("email").send_keys("hello@example.com")
        self.driver.find_element_by_name("org_name").send_keys("Google")
        self.driver.find_element_by_name("password1").send_keys("1234")
        self.driver.find_element_by_name("password2").send_keys("1234")
        se = Select(self.driver.find_element_by_name("profession"))
        se.select_by_index(1)
        self.driver.find_element_by_id("submit").click()
        password2_message_actual = self.driver.find_element_by_id(
            "password2-error"
        ).text
        self.assertEquals(
            "This password is too short. It must contain at least 8 characters.\nThis password is too common.\nThis password is entirely numeric.",
            password2_message_actual,
        )

    def test_register_profession_shortnonnumericpassword(self):
        self.driver.get("http://127.0.0.1:8000/signup/")
        self.driver.find_element_by_name("username").send_keys("mathew")
        self.driver.find_element_by_name("email").send_keys("mathew@example.com")
        self.driver.find_element_by_name("org_name").send_keys("Google")
        self.driver.find_element_by_name("password1").send_keys("Mat@123")
        self.driver.find_element_by_name("password2").send_keys("Mat@123")
        se = Select(self.driver.find_element_by_name("profession"))
        se.select_by_index(1)
        self.driver.find_element_by_id("submit").click()
        password2_message_actual = self.driver.find_element_by_id(
            "password2-error"
        ).text
        self.assertEquals(
            "This password is too short. It must contain at least 8 characters.",
            password2_message_actual,
        )

    def test_register_loginbutton(self):
        self.driver.get("http://127.0.0.1:8000/signup/")
        self.driver.find_element_by_link_text("Sign In").click()
        self.assertEquals(self.driver.title, "Login")

    def test_register_profession_noselect(self):
        self.driver.get("http://127.0.0.1:8000/signup/")
        self.driver.find_element_by_name("username").send_keys("Jack")
        self.driver.find_element_by_name("email").send_keys("jack@example.com")
        self.driver.find_element_by_name("org_name").send_keys("Google")
        self.driver.find_element_by_name("password1").send_keys("Jac#5690")
        self.driver.find_element_by_name("password2").send_keys("Jac#5690")
        self.driver.find_element_by_id("submit").click()
        profession_message_actual = self.driver.find_element_by_id(
            "profession-error"
        ).text
        self.assertEquals("This field is required.", profession_message_actual)

    @classmethod
    def tearDownClass(self):
        pass

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
