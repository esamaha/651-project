from selenium.webdriver.chrome.options import Options
import unittest
import random
from project_post import models as p1
from bid_post import models as b1
from selenium import webdriver
from django.contrib.auth.models import User, Group
from django.urls import reverse, resolve
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BidModifyUITest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("\nTesting bidmodify_ui")

    def setUp(self):
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)

        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()

            datadb = p1.Project.objects.get(Name="DemoProj1")
            biddb = b1.Bid.objects.get(Description="desc1")
            biddb.delete()
            datadb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted

        valid_user = User.objects.create(username="navjeet")
        valid_user.set_password("navj@123")
        a = Group.objects.get(name="client")
        a.user_set.add(valid_user)
        valid_user.save()
        valid_user1 = User.objects.create(username="manak")
        valid_user1.set_password("mana@123")
        b = Group.objects.get(name="bidder")
        b.user_set.add(valid_user1)
        valid_user1.save()

    def test_modify_projectName(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_post/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "Name"))
        )
        self.driver.find_element_by_name("Name").send_keys("DemoProj1")
        self.driver.find_element_by_name("Description").send_keys("test demo proj")
        self.driver.find_element_by_name("Duedate").click()
        self.driver.find_element_by_name("Duedate").send_keys("10-12-2022")
        self.driver.find_element_by_id("submit").click()
        self.driver.find_element_by_name("logout-btn").click()
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("manak")
        self.driver.find_element_by_name("password").send_keys("mana@123")
        self.driver.find_element_by_id("submit").click()
        self.driver.get("http://127.0.0.1:8000/project_list/")
        # WebDriverWait(self.driver, 5).until(
        #     EC.element_to_be_clickable((By.ID, "example"))
        # )
        num_rows = len(
            self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr')
        )
        num_cols = len(
            self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr[1]/td')
        )
        if num_rows == 0:
            pass
        elif num_rows == 1:
            if (
                self.driver.find_element_by_xpath('//*[@id="example"]/tbody/tr/td').text
                == "No data available in table"
            ):
                self.assertEquals(
                    self.driver.find_element_by_xpath(
                        '//*[@id="example"]/tbody/tr/td'
                    ).text,
                    "No data available in table",
                )
            else:
                # specific_col = random.randint(1, num_cols)
                # specific_col = 5
                before_XPath = '//*[@id="example"]/tbody/tr['
                aftertd_XPath = "]/td["
                aftertr_XPath = "]/a"
                FinalXPath = (
                    before_XPath
                    + str(num_rows)
                    + aftertd_XPath
                    + str(num_cols)
                    + aftertr_XPath
                )
                datadb = p1.Project.objects.get(Name="DemoProj1")
                projid = datadb.Project_id
                # self.driver.find_element_by_xpath(FinalXPath).click()
                self.driver.get("http://127.0.0.1:8000/bid_post/?id=" + str(projid))
                WebDriverWait(self.driver, 5).until(
                    EC.element_to_be_clickable((By.NAME, "Cost"))
                )
                self.driver.find_element_by_name("Cost").send_keys("9909")
                self.driver.find_element_by_name("Time").click()
                self.driver.find_element_by_name("Time").send_keys("10-12-2022")
                self.driver.find_element_by_name("Description").send_keys("desc1")
                self.driver.find_element_by_id("submit").click()
                # WebDriverWait(self.driver, 5).until(
                #     EC.element_to_be_clickable((By.ID, "example"))
                # )
                self.driver.get("http://127.0.0.1:8000/bid_list/")
                num_rows1 = len(
                    self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr')
                )
                if num_rows1 == 0:
                    pass
                elif num_rows1 == 1:
                    if (
                        self.driver.find_element_by_xpath(
                            '//*[@id="example"]/tbody/tr/td'
                        ).text
                        == "No data available in table"
                    ):
                        self.assertEquals(
                            self.driver.find_element_by_xpath(
                                '//*[@id="example"]/tbody/tr/td'
                            ).text,
                            "No data available in table",
                        )
                    else:
                        num_cols1 = len(
                            self.driver.find_elements_by_xpath(
                                '//*[@id="example"]/tbody/tr[1]/td'
                            )
                        )
                        before_XPath = '//*[@id="example"]/tbody/tr['
                        aftertd_XPath = "]/td["
                        aftertr_XPath = "]/a"
                        FinalXPath = (
                            before_XPath
                            + str(num_rows1)
                            + aftertd_XPath
                            + str(num_cols1)
                            + aftertr_XPath
                        )
                        datadb1 = b1.Bid.objects.get(Project_id=projid)
                        bidid = datadb1.Bid_id
                        # self.driver.find_element_by_xpath(FinalXPath).click()
                        self.driver.get(
                            "http://127.0.0.1:8000/bid_modify/?id=" + str(bidid)
                        )
                        WebDriverWait(self.driver, 5).until(
                            EC.element_to_be_clickable((By.NAME, "Cost"))
                        )
                        self.driver.find_element_by_name("Cost").send_keys("1")
                        self.driver.find_element_by_id("submit").click()
                        self.driver.get("http://127.0.0.1:8000/bid_list/")
                        # WebDriverWait(self.driver, 5).until(
                        #     EC.element_to_be_clickable((By.ID, "example"))
                        # )
                        col1 = 1
                        before_XPath = '//*[@id="example"]/tbody/tr['
                        aftertd_XPath = "]/td["
                        aftertr_XPath = "]/a"
                        FinalXPath = (
                            before_XPath
                            + str(num_rows1)
                            + aftertd_XPath
                            + str(col1)
                            + aftertr_XPath
                        )
                        self.assertEquals(
                            self.driver.find_element_by_xpath(
                                '//*[@id="example"]/tbody/tr/td'
                            ).text,
                            "19909",
                        )

                else:
                    temp_col = 1
                    bidid = 1
                    specific_row1 = random.randint(1, num_rows1)
                    num_cols2 = len(
                        self.driver.find_elements_by_xpath(
                            '//*[@id="example"]/tbody/tr[1]/td'
                        )
                    )
                    before_XPath = '//*[@id="example"]/tbody/tr['
                    aftertd_XPath = "]/td["
                    aftertr_XPath = "]/a"
                    for i in range(1, num_rows1 + 1):
                        TempPath = (
                            before_XPath
                            + str(i)
                            + aftertd_XPath
                            + str(temp_col)
                            + aftertr_XPath
                        )
                        if self.driver.find_element_by_xpath(TempPath).text == 9909:
                            specific_row1 = i
                            datadb1 = b1.Bid.objects.get(Project_id=projid)
                            bidid = datadb1.Bid_id
                            break
                    FinalXPath = (
                        before_XPath
                        + str(specific_row1)
                        + aftertd_XPath
                        + str(num_cols2)
                        + aftertr_XPath
                    )
                    new_col = 1
                    FinalXPathnew = (
                        before_XPath
                        + str(specific_row1)
                        + aftertd_XPath
                        + str(new_col)
                        + aftertr_XPath
                    )
                    old_cost = self.driver.find_element_by_xpath(FinalXPathnew).text
                    # self.driver.find_element_by_xpath(FinalXPath).click()
                    self.driver.get(
                        "http://127.0.0.1:8000/bid_modify/?id=" + str(bidid)
                    )
                    WebDriverWait(self.driver, 5).until(
                        EC.element_to_be_clickable((By.NAME, "Cost"))
                    )
                    self.driver.find_element_by_name("Cost").send_keys("1")
                    self.driver.find_element_by_id("submit").click()
                    self.driver.get("http://127.0.0.1:8000/bid_list/")
                    # WebDriverWait(self.driver, 5).until(
                    #     EC.element_to_be_clickable((By.ID, "example"))
                    # )
                    col2 = 1
                    before_XPath = '//*[@id="example"]/tbody/tr['
                    aftertd_XPath = "]/td["
                    aftertr_XPath = "]/a"
                    FinalXPath = (
                        before_XPath
                        + str(specific_row1)
                        + aftertd_XPath
                        + str(col2)
                        + aftertr_XPath
                    )
                    self.assertEquals(
                        self.driver.find_element_by_xpath(
                            '//*[@id="example"]/tbody/tr/td'
                        ).text,
                        "1" + old_cost,
                    )

        else:
            specific_row2 = random.randint(1, num_rows)
            temp_col = 1
            projid = 1
            bidid = 1
            # specific_col = random.randint(1,num_cols)
            specific_col2 = 5
            before_XPath = '//*[@id="example"]/tbody/tr['
            aftertd_XPath = "]/td["
            aftertr_XPath = "]/a"
            before_XPath1 = '//*[@id="example"]/tbody/tr['
            aftertd_XPath1 = "]/td["
            aftertr_XPath1 = "]"
            for i in range(1, num_rows + 1):
                TempPath = (
                    before_XPath1
                    + str(i)
                    + aftertd_XPath1
                    + str(temp_col)
                    + aftertr_XPath1
                )
                if self.driver.find_element_by_xpath(TempPath).text == "DemoProj1":
                    specific_row2 = i
                    datadb = p1.Project.objects.get(Name="DemoProj1")
                    projid = datadb.Project_id
                    break
            FinalXPath = (
                before_XPath
                + str(specific_row2)
                + aftertd_XPath
                + str(specific_col2)
                + aftertr_XPath
            )
            # self.driver.find_element_by_xpath(FinalXPath).click()
            self.driver.get("http://127.0.0.1:8000/bid_post/?id=" + str(projid))
            WebDriverWait(self.driver, 5).until(
                EC.element_to_be_clickable((By.NAME, "Cost"))
            )
            self.driver.find_element_by_name("Cost").send_keys("9909")
            self.driver.find_element_by_name("Time").click()
            self.driver.find_element_by_name("Time").send_keys("10-12-2022")
            self.driver.find_element_by_name("Description").send_keys("desc1")
            self.driver.find_element_by_id("submit").click()
            self.driver.get("http://127.0.0.1:8000/bid_list/")
            # WebDriverWait(self.driver, 5).until(
            #     EC.element_to_be_clickable((By.ID, "example"))
            # )
            num_rows1 = len(
                self.driver.find_elements_by_xpath('//*[@id="example"]/tbody/tr')
            )
            if num_rows1 == 0:
                pass
            elif num_rows1 == 1:
                if (
                    self.driver.find_element_by_xpath(
                        '//*[@id="example"]/tbody/tr/td'
                    ).text
                    == "No data available in table"
                ):
                    self.assertEquals(
                        self.driver.find_element_by_xpath(
                            '//*[@id="example"]/tbody/tr/td'
                        ).text,
                        "No data available in table",
                    )
                else:
                    num_cols1 = len(
                        self.driver.find_elements_by_xpath(
                            '//*[@id="example"]/tbody/tr[1]/td'
                        )
                    )
                    before_XPath = '//*[@id="example"]/tbody/tr['
                    aftertd_XPath = "]/td["
                    aftertr_XPath = "]/a"
                    FinalXPath = (
                        before_XPath
                        + str(num_rows1)
                        + aftertd_XPath
                        + str(num_cols1)
                        + aftertr_XPath
                    )
                    # self.driver.find_element_by_xpath(FinalXPath).click()
                    datadb1 = b1.Bid.objects.get(Project_id=projid)
                    bidid = datadb1.Bid_id
                    self.driver.get(
                        "http://127.0.0.1:8000/bid_modify/?id=" + str(bidid)
                    )
                    WebDriverWait(self.driver, 5).until(
                        EC.element_to_be_clickable((By.NAME, "Cost"))
                    )
                    self.driver.find_element_by_name("Cost").send_keys("1")
                    self.driver.find_element_by_id("submit").click()
                    self.driver.get("http://127.0.0.1:8000/bid_list/")
                    # WebDriverWait(self.driver, 5).until(
                    #     EC.element_to_be_clickable((By.ID, "example"))
                    # )
                    col1 = 1
                    before_XPath = '//*[@id="example"]/tbody/tr['
                    aftertd_XPath = "]/td["
                    aftertr_XPath = "]"
                    FinalXPath = (
                        before_XPath
                        + str(num_rows1)
                        + aftertd_XPath
                        + str(col1)
                        + aftertr_XPath
                    )
                    self.assertEquals(
                        self.driver.find_element_by_xpath(
                            '//*[@id="example"]/tbody/tr/td'
                        ).text,
                        "19909",
                    )

            else:
                temp_col = 1
                specific_row1 = random.randint(1, num_rows1)
                num_cols2 = len(
                    self.driver.find_elements_by_xpath(
                        '//*[@id="example"]/tbody/tr[1]/td'
                    )
                )
                before_XPath = '//*[@id="example"]/tbody/tr['
                aftertd_XPath = "]/td["
                aftertr_XPath = "]/a"
                aftertr_XPath2 = "]"
                for i in range(1, num_rows1 + 1):
                    TempPath = (
                        before_XPath
                        + str(i)
                        + aftertd_XPath
                        + str(temp_col)
                        + aftertr_XPath2
                    )
                    if self.driver.find_element_by_xpath(TempPath).text == 9909:
                        specific_row1 = i
                        datadb1 = b1.Bid.objects.get(Project_id=projid)
                        bidid = datadb1.Bid_id
                        break
                FinalXPath = (
                    before_XPath
                    + str(specific_row1)
                    + aftertd_XPath
                    + str(num_cols2)
                    + aftertr_XPath
                )
                new_col = 1
                FinalXPathnew = (
                    before_XPath
                    + str(specific_row1)
                    + aftertd_XPath
                    + str(new_col)
                    + aftertr_XPath
                )
                old_cost = self.driver.find_element_by_xpath(FinalXPathnew).text
                # self.driver.find_element_by_xpath(FinalXPath).click()
                self.driver.get("http://127.0.0.1:8000/bid_modify/?id=" + str(bidid))
                WebDriverWait(self.driver, 5).until(
                    EC.element_to_be_clickable((By.NAME, "Cost"))
                )
                self.driver.find_element_by_name("Cost").send_keys("1")
                self.driver.find_element_by_id("submit").click()
                self.driver.get("http://127.0.0.1:8000/bid_list/")
                # WebDriverWait(self.driver, 5).until(
                #     EC.element_to_be_clickable((By.ID, "example"))
                # )
                col2 = 1
                before_XPath = '//*[@id="example"]/tbody/tr['
                aftertd_XPath = "]/td["
                aftertr_XPath = "]"
                FinalXPath = (
                    before_XPath
                    + str(specific_row1)
                    + aftertd_XPath
                    + str(col2)
                    + aftertr_XPath
                )
                self.assertEquals(
                    self.driver.find_element_by_xpath(
                        '//*[@id="example"]/tbody/tr/td'
                    ).text,
                    "1" + old_cost,
                )

    @classmethod
    def tearDownClass(self):
        try:
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
            userdb1 = User.objects.get(username="manak")
            userdb1.delete()
            datadb = p1.Project.objects.get(Name="DemoProj1")
            biddb = b1.Bid.objects.get(Description="desc1")
            biddb.delete()
            datadb.delete()
            # biddb = models.Bid.objects.all()
            # e = biddb.get(description="desc1")
            # e.delete()
        except:
            pass

    def tearDown(self):
        self.driver.quit()
        try:  # in case not deleted by test

            userdb = User.objects.get(username="navjeet")
            userdb.delete()
            userdb1 = User.objects.get(username="manak")
            userdb1.delete()
            datadb = p1.Project.objects.get(Name="DemoProj1")
            biddb = b1.Bid.objects.get(Description="desc1")
            biddb.delete()
            datadb.delete()
        except:
            pass  # already deleted


if __name__ == "__main__":
    unittest.main()
