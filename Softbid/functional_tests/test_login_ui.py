from selenium.webdriver.chrome.options import Options
import unittest
from selenium import webdriver
from project_post import models
from django.contrib.auth.models import User, Group
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class loginUiTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("\nTesting login_ui")

    def setUp(self):
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)

        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted

        valid_user = User.objects.create(username="navjeet")
        valid_user.set_password("navj@123")
        a = Group.objects.get(name="client")
        a.user_set.add(valid_user)
        valid_user.save()

    def test_login_nousername(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("")
        self.driver.find_element_by_name("password").send_keys("hello@991")
        self.driver.find_element_by_id("submit").click()
        self.assertEquals(
            self.driver.find_element_by_id("error").text, "Invalid inputs"
        )

    def test_login_nopassword(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("john")
        self.driver.find_element_by_name("password").send_keys("")
        self.driver.find_element_by_id("submit").click()
        self.assertEquals(
            self.driver.find_element_by_id("error").text, "Invalid inputs"
        )

    def test_login_invalidcredentials(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("john")
        self.driver.find_element_by_name("password").send_keys("hello@991")
        self.driver.find_element_by_id("submit").click()
        self.assertEquals(
            self.driver.find_element_by_id("error").text, "Invalid username or password"
        )

    def test_login_joinbutton(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_link_text("Join now").click()
        self.assertEquals(self.driver.title, "Signup")

    def test_login_validcredentials1(self):
        self.driver.get("http://127.0.0.1:8000/login/")
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.NAME, "username"))
        )
        self.driver.find_element_by_name("username").send_keys("navjeet")
        self.driver.find_element_by_name("password").send_keys("navj@123")
        self.driver.find_element_by_id("submit").click()
        self.assertNotEquals(self.driver.title, "Login")

    @classmethod
    def tearDownClass(self):
        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
        except:
            pass  # already deleted

    def tearDown(self):
        self.driver.quit()
        try:  # in case not deleted by test
            userdb = User.objects.get(username="navjeet")
            userdb.delete()
        except:
            pass  # already deleted


if __name__ == "__main__":
    unittest.main()
