from unittest import TestCase
import time, random, string
from django.contrib.auth.models import User, Group
from django.urls import reverse, resolve

# Selenium
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait  # instead of fixed delay
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By

base_url = "http://127.0.0.1:8000"
login_url = reverse("login")
account_url = reverse("account-page")

# region Helper Func
def RandString(size=10):
    return "".join(random.choices(string.ascii_letters + string.digits, k=size))


def CreateUser():  # I don't know why in classmethod the database is not flushed and checks if username is unique with other entries => need to create unique test username to avoid error of duplicate user
    try:
        test_username = RandString()
        valid_user = User.objects.create(username=test_username)
        # This will hash the password to be compared later on in Login
        valid_user.set_password("edy")
        valid_user.save()
        return {"username": test_username, "password": "edy"}
    except:
        return CreateUser()


# endregion

# NOTE: SINCE THIS IS A UI TEST, REMEMBER TO RUNSERVER FIRST
class EditTest(TestCase):
    @classmethod
    def setUpClass(cls):  # Before ALL tests
        print("Testing account_ui")
        # Creating account
        cls.test_user = CreateUser()

        # setup driver
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        cls.driver = webdriver.Chrome(chrome_options=options)
        cls.wdwait = WebDriverWait(cls.driver, 4)

        # Login
        cls.driver.get(base_url + login_url)
        elem = cls.wdwait.until(
            ec.visibility_of_element_located((By.ID, "username"))
        )  # cls.driver.find_element_by_id('username')
        elem.send_keys(cls.test_user["username"])
        elem = cls.driver.find_element_by_id("password")
        elem.send_keys(cls.test_user["password"])
        elem.send_keys(Keys.RETURN)  # press Enter
        # print("Loggedin")

    def setUp(self):  # before EACH test
        EditTest.driver.get(base_url + account_url)  # goto account

    # test the dynamic setting of the 'class="curent-page"' attribute to the nav bar elements
    def test_Edit_GET(self):
        elem = EditTest.wdwait.until(
            ec.visibility_of_element_located((By.ID, "greeting"))
        )  # cls.driver.find_element_by_id('username')

        self.assertIn(
            EditTest.test_user["username"],
            elem.text,
            msg="Username NOT found in Greeting message of Edit page",
        )

    def test_Edit_POST_Trivial(self):
        testemail = "testemail@softbid.com"
        testcompany = "Microsoft"
        elem = EditTest.wdwait.until(
            ec.visibility_of_element_located((By.NAME, "email"))
        )  # cls.driver.find_element_by_id('username')
        elem.clear()  # Important coz might have data from previous tests stored
        elem.send_keys(testemail)
        elem = EditTest.driver.find_element_by_name("first_name")
        elem.clear()  # Important coz might have data from previous tests stored
        elem.send_keys(testcompany)

        # Submit
        # press Enter #EditTest.driver.post(base_url+account_url) #no post method in webdirver but no need anyway
        elem.send_keys(Keys.RETURN)
        # check if page refreshed (post)
        try:
            elem = EditTest.wdwait.until(
                ec.visibility_of_element_located((By.CLASS_NAME, "success"))
            )  # cls.driver.find_element_by_id('username')
        except:
            raise AssertionError("Succes message NOT shown after trivial Edit")

        # Check if new data saved
        emaildata = EditTest.driver.find_element_by_name("email")
        emaildata = emaildata.get_attribute("value")
        companydata = EditTest.driver.find_element_by_name("first_name")
        companydata = companydata.get_attribute("value")
        self.assertEquals(
            testemail,
            emaildata,
            msg="Email data in form NOT saved after valid Post edit",
        )
        self.assertEquals(
            testcompany,
            companydata,
            msg="Company data in form NOT saved after valid Post edit",
        )

    def test_Edit_POST_CorrectPass(self):
        testpass = "Password123"
        # testpass_confirm = testpass #no need
        elem = EditTest.wdwait.until(
            ec.visibility_of_element_located((By.NAME, "newpass"))
        )  # cls.driver.find_element_by_id('username')
        elem.send_keys(testpass)
        elem = EditTest.driver.find_element_by_name("pass_confirmation")
        elem.send_keys(testpass)

        # Submit
        # press Enter #EditTest.driver.post(base_url+account_url) #no post method in webdirver but no need anyway
        elem.send_keys(Keys.RETURN)
        # check if page refreshed (post)
        try:
            elem = EditTest.wdwait.until(
                ec.visibility_of_element_located((By.CLASS_NAME, "success"))
            )  # cls.driver.find_element_by_id('username')
        except:
            raise AssertionError("Succes message NOT shown after Password Edit")

        # check if password changed
        userdb = User.objects.get(username=EditTest.test_user["username"])
        self.assertTrue(
            userdb.check_password(testpass), msg="Password NOT correctly changed"
        )
        # Update Global dict just in case other tests happen after this one so they use the updated pass instead of old one
        EditTest.test_user["password"] = testpass

    def test_Edit_POST_CorrectPass_andTrivial(self):
        # change pass
        testpass = "Password123"
        # testpass_confirm = testpass #no need
        elem = EditTest.wdwait.until(
            ec.visibility_of_element_located((By.NAME, "newpass"))
        )  # cls.driver.find_element_by_id('username')
        elem.send_keys(testpass)
        elem = EditTest.driver.find_element_by_name("pass_confirmation")
        elem.send_keys(testpass)

        # change trivial
        testemail = "testemail2@softbid.com"  # make sure testemail is different in all cases coz might contain data from previous test
        elem = EditTest.driver.find_element_by_name("email")
        elem.clear()  # Important coz might have data from previous tests stored
        elem.send_keys(testemail)

        # Submit
        # press Enter #EditTest.driver.post(base_url+account_url) #no post method in webdirver but no need anyway
        elem.send_keys(Keys.RETURN)
        # check if page refreshed (post)
        try:
            elem = EditTest.wdwait.until(
                ec.visibility_of_element_located((By.CLASS_NAME, "success"))
            )  # cls.driver.find_element_by_id('username')
        except:
            raise AssertionError(
                "Succes message NOT shown after Password and other data Edit"
            )
        # check if trivial changed
        emaildata = EditTest.driver.find_element_by_name("email")
        emaildata = emaildata.get_attribute("value")
        self.assertEquals(
            testemail,
            emaildata,
            msg="Email data in form NOT saved after valid Password Change",
        )
        # check if password changed
        userdb = User.objects.get(username=EditTest.test_user["username"])
        self.assertTrue(
            userdb.check_password(testpass),
            msg="Password NOT correctly changed",
        )
        # Update Global dict just in case other tests happen after this one so they use the updated pass instead of old one
        EditTest.test_user["password"] = testpass

    def test_Edit_POST_ShortPass(self):
        testpass = "L2p"
        # testpass_confirm = testpass #no need
        elem = EditTest.wdwait.until(
            ec.visibility_of_element_located((By.NAME, "newpass"))
        )  # cls.driver.find_element_by_id('username')
        elem.send_keys(testpass)
        elem = EditTest.driver.find_element_by_name("pass_confirmation")
        elem.send_keys(testpass)

        # Submit
        # press Enter #EditTest.driver.post(base_url+account_url) #no post method in webdirver but no need anyway
        elem.send_keys(Keys.RETURN)
        # check if page refreshed (post)
        try:
            elem = EditTest.wdwait.until(
                ec.visibility_of_element_located((By.CLASS_NAME, "errorlist"))
            )  # cls.driver.find_element_by_id('username')
        except:
            raise AssertionError("Password too short message not shown after post edit")

        # check if password is the same
        userdb = User.objects.get(username=EditTest.test_user["username"])
        self.assertTrue(
            userdb.check_password(EditTest.test_user["password"]),
            msg="Short password accepted",
        )

    def test_Edit_POST_BadConfirmationPass(self):
        testpass = "Password123"
        testpass_confirm = "password123"
        elem = EditTest.wdwait.until(
            ec.visibility_of_element_located((By.NAME, "newpass"))
        )  # cls.driver.find_element_by_id('username')
        elem.send_keys(testpass)
        elem = EditTest.driver.find_element_by_name("pass_confirmation")
        elem.send_keys(testpass_confirm)

        # Submit
        # press Enter #EditTest.driver.post(base_url+account_url) #no post method in webdirver but no need anyway
        elem.send_keys(Keys.RETURN)
        # check if page refreshed (post)
        try:
            elem = EditTest.wdwait.until(
                ec.visibility_of_element_located((By.CLASS_NAME, "errorlist"))
            )  # cls.driver.find_element_by_id('username')
        except:
            raise AssertionError("Password unmatch not shown after post edit")

        # check if password is the same
        userdb = User.objects.get(username=EditTest.test_user["username"])
        self.assertTrue(
            userdb.check_password(EditTest.test_user["password"]),
            msg="Bad password confirmation accepted",
        )

    @classmethod
    def tearDownClass(cls):  # after ALL test
        cls.driver.quit()
        # delete test user
        userdb = User.objects.get(username=EditTest.test_user["username"])
        userdb.delete()


class DeleteTest(TestCase):
    def setUp(self):  # before EACH test
        # Creating account
        self.test_user = CreateUser()

        # setup driver
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)
        self.wdwait = WebDriverWait(self.driver, 3)

        # Login
        self.driver.get(base_url + login_url)
        elem = self.wdwait.until(
            ec.visibility_of_element_located((By.ID, "username"))
        )  # self.driver.find_element_by_id('username')
        elem.send_keys(self.test_user["username"])
        elem = self.driver.find_element_by_id("password")
        elem.send_keys(self.test_user["password"])
        elem.send_keys(Keys.RETURN)  # press Enter
        # print("Loggedin")
        # goto account
        self.driver.get(base_url + account_url)

    # NOTE: Seperate class coz conflict with teardown of editclass since deleteing same user in both places
    def test_Delete_Account(self):
        delbtn = self.wdwait.until(ec.visibility_of_element_located((By.ID, "delbtn")))
        delbtn.click()
        # confirm
        confirm_delbtn = self.wdwait.until(
            ec.visibility_of_element_located((By.ID, "confirm_delbtn"))
        )
        confirm_delbtn.click()

        # checking if account deleted
        try:
            User.objects.get(username=self.test_user["username"])
            raise AssertionError(
                "User was NOT deleted in db after proper delete confirmation"
            )
        except:  # User.DoesNotExist: #not finding it
            pass  # he was deleted

    def tearDown(self):
        self.driver.quit()
        # delete test user
        try:  # in case not deleted by test
            userdb = User.objects.get(username=self.test_user["username"])
            userdb.delete()
        except:
            # print("Already deleted")
            pass  # already deleted
